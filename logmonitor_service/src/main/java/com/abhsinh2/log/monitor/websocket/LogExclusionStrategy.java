package com.abhsinh2.log.monitor.websocket;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

public class LogExclusionStrategy implements ExclusionStrategy {
    public static final Set<String> exclusionFieldsSet = new HashSet<>(Collections.singletonList("image"));

    @Override
    public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        return exclusionFieldsSet.contains(fieldAttributes.getName());
    }

    @Override
    public boolean shouldSkipClass(Class<?> aClass) {
        return false;
    }
}
