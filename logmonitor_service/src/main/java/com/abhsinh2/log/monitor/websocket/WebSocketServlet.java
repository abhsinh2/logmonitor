package com.abhsinh2.log.monitor.websocket;

import java.util.HashMap;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.abhsinh2.log.monitor.config.logger.LogMonitorLogger;
import com.google.gson.Gson;

@ServerEndpoint(value = "/websocket/logmonitor")
public class WebSocketServlet {
    private static final Logger LOGGER = LogMonitorLogger.getLogger(WebSocketServlet.class);

    @Autowired
    private WebSocketManager webSocketManager;

    private Gson gson = TransformerUtility.getGson();

    public WebSocketServlet() {
    }

    @OnOpen
    public void open(Session session) {
        webSocketManager.addSession(session);

        // Sends session id to the client
        HashMap<String, String> res = new HashMap<>();
        res.put("connectionId", session.getId());
        webSocketManager.sendTo(session, gson.toJson(res));
    }

    @OnClose
    public void close(Session session) {
        webSocketManager.removeSession(session);
    }

    @OnError
    public void onError(Throwable error) {
        LOGGER.error("onError:", error);
    }

    @OnMessage
    public void handleMessage(String message, Session session) {
        LOGGER.debug("handleMessage:" + message + " sessionId:" + session.getId());
    }
}
