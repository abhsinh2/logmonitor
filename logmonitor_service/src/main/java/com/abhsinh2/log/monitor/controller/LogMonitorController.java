package com.abhsinh2.log.monitor.controller;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import com.abhsinh2.log.monitor.config.Application;
import com.abhsinh2.log.monitor.config.logger.LogMonitorLogger;

/**
 *
 * @author abhsinh2
 *
 */
@Component
public class LogMonitorController {

    private static final Logger LOGGER = LogMonitorLogger.getLogger(LogMonitorController.class);

    @PostConstruct
    public void init() {
        LOGGER.info("Initiliatizing LogMonitor");
        System.setProperty(Application.SYSTEM_PROPERTY_CONFIG_FILE,
                "/Users/abhsinh2/Abhishek/bitbucket/logmonitor/logmonitor/src/test/resources/conf/log_regex_config.json");
    }
}
