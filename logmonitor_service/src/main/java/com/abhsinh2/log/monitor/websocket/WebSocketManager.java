package com.abhsinh2.log.monitor.websocket;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.websocket.Session;

import org.springframework.stereotype.Component;

@Component
public class WebSocketManager {
    private final Set<Session> sessions = new CopyOnWriteArraySet<>();

    public boolean hasSessions() {
        return sessions.size() > 0;
    }

    public void addSession(Session session) {
        sessions.add(session);
    }

    public void removeSession(Session session) {
        sessions.remove(session);
    }

    public Set<Session> getSessions() {
        return sessions;
    }

    public void sendToAll(String message, List<String> excludeSessionIds) {
        for (Session session : sessions) {
            if (excludeSessionIds == null || !excludeSessionIds.contains(session.getId())) {
                sendTo(session, message);
            }
        }
    }

    public void sendTo(Session session, String message) {
        try {
            session.getBasicRemote().sendText(message);
        } catch (IOException e) {
            removeSession(session);
        }
    }

}
