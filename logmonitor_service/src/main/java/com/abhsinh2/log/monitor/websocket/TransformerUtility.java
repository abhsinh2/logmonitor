package com.abhsinh2.log.monitor.websocket;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class TransformerUtility {
    public static Gson getGson() {
        return new GsonBuilder().setExclusionStrategies(new LogExclusionStrategy()).serializeNulls().create();
    }
}
