package com.abhsinh2.log.monitor.controller;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.abhsinh2.log.monitor.config.Application;
import com.abhsinh2.log.monitor.config.Configuration;
import com.abhsinh2.log.monitor.config.logger.LogMonitorLogger;
import com.abhsinh2.log.monitor.formatter.HtmlFormatter;
import com.abhsinh2.log.monitor.handler.FileHandler;
import com.abhsinh2.log.monitor.handler.Handler;
import com.abhsinh2.log.monitor.parser.listener.ILogMessageListener;
import com.abhsinh2.log.monitor.parser.model.Record;
import com.abhsinh2.log.monitor.service.FileMonitorService;

/**
 *
 * @author abhsinh2
 *
 */
@Controller
@RequestMapping("/ws")
public class LogMonitorRestController {
    private static Map<String, FileMonitorService> logservices = new ConcurrentHashMap<>();
    private static final Logger LOGGER = LogMonitorLogger.getLogger(LogMonitorController.class);

    @RequestMapping(value = "/rest/start", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody String start(@RequestBody String message) throws Exception {
        System.out.println("Starting logmonitor service:" + message);
        UUID id = UUID.randomUUID();

        Handler htmlFileHandler = new FileHandler("/root/abc.html");
        htmlFileHandler.setFormatter(new HtmlFormatter("Test"));

        ILogMessageListener listener = new ILogMessageListener() {
            @Override
            public void listen(URI uri, Record record) {
                LOGGER.debug(record.getLog());
            }
        };

        List<Handler> handlers = new ArrayList<>();
        List<ILogMessageListener> listeners = new ArrayList<>();

        handlers.add(htmlFileHandler);
        listeners.add(listener);

        String configFilePath = System.getProperty(Application.SYSTEM_PROPERTY_CONFIG_FILE);
        Configuration config = new Configuration(new File(configFilePath));

        FileMonitorService logMonitorService = new FileMonitorService(config, null);
        logMonitorService.setHandlers(handlers);
        logMonitorService.setListeners(listeners);

        logservices.put(id.toString(), logMonitorService);

        logMonitorService.handleMessage(message);
        return id.toString();
    }

    @RequestMapping(value = "/rest/stop", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody String stop(@RequestParam(value = "id") String id) throws Exception {
        System.out.println("Stopping logmonitor service " + id);
        FileMonitorService logMonitorService = logservices.get(id);

        String message = "{action:\"stop\"}";
        if (logMonitorService != null) {
            logMonitorService.handleMessage(message);
        }
        return "Stopped";
    }
}
