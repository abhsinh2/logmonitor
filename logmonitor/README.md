
# Introduction

This framework parses log files with given parsers and returns tokens via listener. The framework also provides APIs to parse a log file within given start and end timestamp. This framework not only parses local file but can tail local or remote local file and parses logs written in file. It also provides a mechanism to watch for a directory (normally logs directory) and parses only those log file which were modified during watcher run.

## Features

* Tail local file
* Tail remote file
* Parse given file
* Watch a local directory and parse logs file which are changed while running watcher.

# Define parsers

Parsers can be defined in configuration file in following ways:
(look for src/main/resources/conf/log_regex_config.json or src/test/resources/conf/log_regex_config.json)

* Using Java Regex


	"name": "default",
	"pattern": "\\[\\s*(.+)\\s*\\]\\s+\\[\\s*(.+)\\s*\\]\\s+\\[\\s*(.+)\\s*\\]\\s+\\[\\s*(.+)\\s*\\]\\s+-\\s+(.+)",
	"dateFormat": "yyyy-MM-dd hh-mm-ss,SSS",
	"dateIndex": 0,
	"labels": ["Date", "Thread", "Component", "Severity", "Message"]


* Using Log4j


	"name": "defaultLog4j",
	"log4jPattern": "[%d] [%t] [%c{1}] [%-5p] - %m%n",
	"labels": ["Date", "Thread", "Component", "Severity", "Message"]


* Using custom Java implementation


	"name": "defaultClass",
	"className": "com.abhsinh2.log.monitor.parser.Log4jMessageParser"


# Associate parsers to log files

In that same configuration file, we can specify which kind of log files will be parsed by which parser


	"filenamePattern": [
		{
			"name": "jobManager.log",
			"parser": "defaultClass"
		},
		{
			"name": "inventory.log",
			"parser": "default"
		}
	]

 

# APIs

## Tail local file

[LocalFileTailerTest](src/test/java/com/abhsinh2/log/monitor/tail/local/LocalFileTailerTest.java)

## Tail remote file

[RemoteFileTailerTest](src/test/java/com/abhsinh2/log/monitor/tail/remote/RemoteFileTailerTest.java)

## Local directory watcher

[DirectoryWatcherServiceTest](src/test/java/com/abhsinh2/log/monitor/watcher/DirectoryWatcherServiceTest.java)

# TODO

* Support SLF4j
* Web application where logs tokens can be send from server to client using websocket
 