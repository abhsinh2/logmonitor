package com.abhsinh2.log.monitor;

import com.abhsinh2.log.monitor.handler.Handler;
import com.abhsinh2.log.monitor.parser.listener.ILogMessageListener;

/**
 *
 * @author abhsinh2
 *
 */
public interface IFileMonitor {
    public void start();

    public void stop();

    public void addListener(ILogMessageListener listener);

    public void removeListener(ILogMessageListener listener);

    public void addHandler(Handler handler);

    public void removeHandler(Handler handler);
}
