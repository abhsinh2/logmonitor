package com.abhsinh2.log.monitor.handler;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.abhsinh2.log.monitor.parser.model.Record;

/**
 *
 * @author abhsinh2
 *
 */
public class FileHandler extends StreamHandler {
    private boolean append;

    public FileHandler(String filePath) throws IOException {
        this(filePath, false);
    }

    public FileHandler(String filePath, boolean append) throws IOException {
        this.append = append;
        open(new File(filePath), this.append);
    }

    private void open(File fname, boolean append) throws IOException {
        FileOutputStream fout = new FileOutputStream(fname.toString(), append);
        BufferedOutputStream bout = new BufferedOutputStream(fout);
        setOutputStream(bout);
    }

    @Override
    public synchronized void publish(Record record) {
        if (!isLoggable(record)) {
            return;
        }
        super.publish(record);
        flush();
    }

}
