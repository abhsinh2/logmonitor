package com.abhsinh2.log.monitor.formatter;

import com.abhsinh2.log.monitor.parser.model.Label;
import com.abhsinh2.log.monitor.parser.model.Record;

/**
 *
 * @author abhsinh2
 *
 */
public class HtmlFormatter extends Formatter {

    public HtmlFormatter(String title) {

    }

    @Override
    public String format(Record record) {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("<tr>\n");

        for (String token : record.getTokens()) {
            builder.append("<td>");
            builder.append(token);
            builder.append("</td>\n");
        }

        builder.append("</tr>\n");
        return builder.toString();
    }

    @Override
    public String getHead(Label label) {
        return "<!DOCTYPE html>\n<head>\n<style>\n" + "table { width: 100% }\n" + "th { font:bold 10pt Tahoma; }\n"
                + "td { font:normal 10pt Tahoma; }\n" + "h1 {font:normal 11pt Tahoma;}\n" + "</style>\n" + "</head>\n"
                + "<body>\n" + "<table border=\"0\" cellpadding=\"5\" cellspacing=\"3\">\n" + "<tr align=\"left\">\n"
                + "\t<th style=\"width:10%\">Time</th>\n" + "\t<th style=\"width:10%\">Class</th>\n"
                + "\t<th style=\"width:10%\">Method</th>\n" + "\t<th style=\"width:10%\">Loglevel</th>\n"
                + "\t<th style=\"width:60%\">Log Message</th>\n" + "</tr>\n";
    }

    @Override
    public String getTail() {
        StringBuilder builder = new StringBuilder();
        builder.append("</table>\n");
        builder.append("</body>\n");
        builder.append("</html>");
        return builder.toString();
    }
}
