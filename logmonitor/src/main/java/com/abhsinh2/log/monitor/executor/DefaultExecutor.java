package com.abhsinh2.log.monitor.executor;

import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author abhsinh2
 *
 */
public class DefaultExecutor extends ThreadPoolExecutor {

    public DefaultExecutor(Properties properties) {
        super(0, 100, 1, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(100, true));
    }

}
