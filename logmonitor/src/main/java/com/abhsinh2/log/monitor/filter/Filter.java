package com.abhsinh2.log.monitor.filter;

import com.abhsinh2.log.monitor.parser.model.Record;

/**
 *
 * @author abhsinh2
 *
 */
public interface Filter {
    public boolean isLoggable(Record record);
}
