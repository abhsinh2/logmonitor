package com.abhsinh2.log.monitor.parser.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.Date;
import java.util.Scanner;

import org.slf4j.Logger;

import com.abhsinh2.log.monitor.config.logger.LogMonitorLogger;
import com.abhsinh2.log.monitor.parser.ILogMessageParser;
import com.abhsinh2.log.monitor.parser.IMessageParser;
import com.abhsinh2.log.monitor.parser.listener.ILogMessageListener;
import com.abhsinh2.log.monitor.parser.model.Label;
import com.abhsinh2.log.monitor.parser.model.Record;

/**
 * Default Log file Parser
 *
 * @author abhsinh2
 *
 */
public class DefaultFileParser implements IFileParser {
    private static final Logger LOGGER = LogMonitorLogger.getLogger(DefaultFileParser.class);

    @Override
    public void listen(File fileTobeParsed, IMessageParser messageParser, ILogMessageListener messageListener)
            throws FileParserException {
        Scanner logScanner = null;
        try {
            logScanner = new Scanner(fileTobeParsed);

            Label label = new Label(messageParser.labels());

            while (logScanner.hasNextLine()) {
                String logString = logScanner.nextLine();
                String[] tokens = messageParser.parse(logString);

                if (messageListener != null) {
                    Record record = new Record(logString, label, tokens);
                    messageListener.listen(fileTobeParsed.toURI(), record);
                }
            }
        } catch (FileNotFoundException e) {
            throw new FileParserException(e.getMessage());
        } finally {
            if (logScanner != null) {
                logScanner.close();
            }
        }
    }

    @Override
    public void listen(File fileTobeParsed, ILogMessageParser messageParser, Date startDate, Date endDate,
            ILogMessageListener messageListener) throws FileParserException {
        Scanner logScanner = null;
        try {
            logScanner = new Scanner(fileTobeParsed);
            Label label = new Label(messageParser.labels());

            while (logScanner.hasNextLine()) {
                String logString = logScanner.nextLine();
                String[] tokens = messageParser.parse(logString);
                int dateIndex = messageParser.getDateIndex();

                if (tokens.length > dateIndex) {
                    String dateString = tokens[dateIndex];
                    Date date = messageParser.getDateFormat().parse(dateString);

                    if (date.after(endDate)) {
                        break;
                    }

                    if (date.before(startDate)) {
                        continue;
                    }

                    if (messageListener != null) {
                        Record record = new Record(logString, label, tokens);
                        messageListener.listen(fileTobeParsed.toURI(), record);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            throw new FileParserException(e.getMessage());
        } catch (ParseException e) {
            throw new FileParserException(e.getMessage());
        } finally {
            if (logScanner != null) {
                logScanner.close();
            }
        }
    }

}
