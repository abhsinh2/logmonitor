package com.abhsinh2.log.monitor.parser.model;

/**
 *
 * @author abhsinh2
 *
 */
public final class Label {
    private String[] labels;

    public Label(String[] labels) {
        this.labels = labels;
    }

    public String[] getLabels() {
        return labels;
    }
}
