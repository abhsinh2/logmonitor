package com.abhsinh2.log.monitor.config.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * Logger class to log message in file.
 *
 * @author abhsinh2
 *
 */
public class LogMonitorLogger {
    private static final Logger ROOT_LOGGER = LoggerFactory.getLogger("log.monitor");

    public static Logger getRootLogger() {
        return ROOT_LOGGER;
    }

    public static Logger getLogger(Class<?> clazz) {
        return LoggerFactory.getLogger(clazz);
    }
}
