package com.abhsinh2.log.monitor.action;

import java.io.IOException;

/**
 *
 * @author abhsinh2
 *
 */
public interface MessageResponder {
    void receive(String messageString) throws IOException;
}
