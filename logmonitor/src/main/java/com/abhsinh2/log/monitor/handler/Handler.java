package com.abhsinh2.log.monitor.handler;

import com.abhsinh2.log.monitor.filter.Filter;
import com.abhsinh2.log.monitor.formatter.Formatter;
import com.abhsinh2.log.monitor.parser.model.Record;

/**
 *
 * @author abhsinh2
 *
 */
public abstract class Handler {
    private volatile Filter filter;
    private volatile Formatter formatter;

    protected Handler() {
    }

    public abstract void publish(Record record);

    public synchronized void setFormatter(Formatter newFormatter) {
        formatter = newFormatter;
    }

    public Formatter getFormatter() {
        return formatter;
    }

    public synchronized void setFilter(Filter newFilter) {
        filter = newFilter;
    }

    public Filter getFilter() {
        return filter;
    }

    public boolean isLoggable(Record record) {
        final Filter filter = getFilter();
        if (filter == null) {
            return true;
        }
        return filter.isLoggable(record);
    }

    public abstract void flush();

    public abstract void close();

}
