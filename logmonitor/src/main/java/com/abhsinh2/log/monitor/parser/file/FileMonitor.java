package com.abhsinh2.log.monitor.parser.file;

import java.io.File;
import java.net.URI;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.abhsinh2.log.monitor.AbstractFileMonitor;
import com.abhsinh2.log.monitor.parser.IMessageParser;
import com.abhsinh2.log.monitor.parser.listener.ILogMessageListener;
import com.abhsinh2.log.monitor.parser.model.Record;

/**
 *
 * @author abhsinh2
 *
 */
public class FileMonitor extends AbstractFileMonitor {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileMonitor.class);

    private FileExecutor fileExecutor;
    private IFileParser fileParser;
    private final AtomicBoolean isCancelled = new AtomicBoolean(false);

    public FileMonitor(URI uri, Executor executor, IMessageParser messageParser) {
        super(uri, executor, messageParser);

        String path = uri.getPath();
        LOGGER.debug("path: {}", path);
        File file = new File(path);
        LOGGER.debug("file: {}", file);
        fileExecutor = new FileExecutor(file);
    }

    @Override
    public void start() {
        if (!isCancelled.get()) {
            executor.execute(fileExecutor);
        }
    }

    @Override
    public void stop() {
        isCancelled.set(true);
        super.stop();
    }

    private class FileExecutor implements Runnable {
        private File file;

        FileExecutor(File file) {
            this.file = file;
            fileParser = new DefaultFileParser();
        }

        @Override
        public void run() {
            try {
                fileParser.listen(this.file, messageParser, new ILogMessageListener() {
                    @Override
                    public void listen(URI uri, Record record) {
                        notifyListener(uri, record);
                    }
                });
            } catch (FileParserException e) {
                e.printStackTrace();
            }
        }
    }

}
