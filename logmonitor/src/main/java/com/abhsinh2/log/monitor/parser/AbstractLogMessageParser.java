package com.abhsinh2.log.monitor.parser;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author abhsinh2
 *
 */
public abstract class AbstractLogMessageParser implements ILogMessageParser {

    protected String[] labels;
    protected String logPattern;
    protected String dateFormat;
    protected int dateIndex;

    public AbstractLogMessageParser(String logPattern, String[] labels, String dateFormat, int dateIndex) {
        this.logPattern = logPattern;
        this.labels = labels;
        this.dateFormat = dateFormat;
        this.dateIndex = dateIndex;
    }

    @Override
    public String[] labels() {
        return this.labels;
    }

    protected Pattern getLogPattern() {
        return Pattern.compile(this.logPattern);
    }

    @Override
    public SimpleDateFormat getDateFormat() {
        return new SimpleDateFormat(this.dateFormat);
    }

    @Override
    public String[] parse(String message) {
        Matcher matcher = getLogPattern().matcher(message);
        String[] str = new String[0];
        String[] labels = labels();

        if (matcher.matches()) {
            if (labels != null && (matcher.groupCount() >= labels.length)) {
                str = new String[matcher.groupCount()];
                for (int i = 0; i < matcher.groupCount(); i++) {
                    str[i] = matcher.group(i + 1);
                }
            }
        }
        return str;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " [labels=" + Arrays.toString(labels) + ", logPattern=" + logPattern
                + ", dateFormat=" + dateFormat + "]";
    }
}
