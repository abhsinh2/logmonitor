package com.abhsinh2.log.monitor.watcher;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import org.slf4j.Logger;

import com.abhsinh2.log.monitor.config.logger.LogMonitorLogger;

/**
 * Watches given directory for any file changes.
 *
 * @author abhsinh2
 *
 */
public class DirectoryWatcher implements IDirectoryWatcher, Runnable {

    private static final Logger LOGGER = LogMonitorLogger.getLogger(DirectoryWatcher.class);

    private WatchService watchService;
    private Set<FileChangeListener> listeners;
    private Map<FileChangeListener, Set<PathMatcher>> listenerToFilePatternsMap;
    private final AtomicBoolean cancelPending = new AtomicBoolean(false);

    private File directoryToWatch;
    private Path directoryPath;
    private Executor executor;

    public DirectoryWatcher(File directory, Executor executor) throws IOException {
        if (!directory.isDirectory()) {
            throw new IllegalArgumentException(directory + " is not a directory.");
        }

        LOGGER.debug("directory {}", directory.getPath());

        this.executor = executor;
        this.directoryToWatch = directory;
        this.directoryPath = Paths.get(this.directoryToWatch.getPath());

        this.watchService = FileSystems.getDefault().newWatchService();
        this.listenerToFilePatternsMap = new ConcurrentHashMap<>();
        this.listeners = Collections.newSetFromMap(new ConcurrentHashMap<>());

        this.directoryPath.register(watchService, ENTRY_CREATE, ENTRY_MODIFY, ENTRY_DELETE);
    }

    @Override
    public File getDirectory() {
        return directoryToWatch;
    }

    @Override
    public void start() {
        LOGGER.debug("Start watching directory {}", this.directoryToWatch);
        if (!cancelPending.get()) {
            LOGGER.debug("Running Thread");
            executor.execute(this);
            // Thread runnerThread = new Thread(this,
            // DirectoryWatcher.class.getSimpleName());
            // runnerThread.start();
        }
    }

    @Override
    public void stop() {
        LOGGER.debug("Stop watching directory {}", this.directoryToWatch);
        cancelPending.set(true);
    }

    @Override
    public void register(FileChangeListener listener) {
        LOGGER.debug("Registering for *");
        register(listener, "*");
    }

    @Override
    public void register(FileChangeListener listener, String... filePatterns) {
        LOGGER.debug("Registering for filePatterns " + filePatterns);

        listeners.add(listener);

        Set<PathMatcher> patterns = Collections.newSetFromMap(new ConcurrentHashMap<>());

        for (String globPattern : filePatterns) {
            patterns.add(matcherForFileExpression(globPattern));
        }

        if (patterns.isEmpty()) {
            patterns.add(matcherForFileExpression("*"));
        }

        listenerToFilePatternsMap.put(listener, patterns);
    }

    private PathMatcher matcherForFileExpression(String filePattern) {
        return FileSystems.getDefault().getPathMatcher("glob:" + filePattern);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void notifyListeners(WatchKey key) {
        LOGGER.debug("Notifying Listernes");

        for (WatchEvent<?> event : key.pollEvents()) {
            WatchEvent.Kind eventKind = event.kind();

            // Overflow occurs when the watch event queue is overflown
            // with events.
            if (eventKind.equals(OVERFLOW)) {
                // TODO: Notify all listeners.
                return;
            }

            WatchEvent<Path> pathEvent = (WatchEvent<Path>) event;
            Path file = pathEvent.context();
            Path child = directoryPath.resolve(file);

            // print out event
            // LOGGER.debug("%s: %s\n", event.kind().name(), child);

            if (eventKind.equals(ENTRY_CREATE)) {
                matchedListeners(file).forEach(listener -> {
                    listener.onFileCreate(new File(this.directoryToWatch.getPath() + "/" + file));
                });
            } else if (eventKind.equals(ENTRY_MODIFY)) {
                matchedListeners(file).forEach(listener -> {
                    listener.onFileModify(new File(this.directoryToWatch.getPath() + "/" + file));
                });
            } else if (eventKind.equals(ENTRY_DELETE)) {
                matchedListeners(file).forEach(listener -> {
                    listener.onFileDelete(new File(this.directoryToWatch.getPath() + "/" + file));
                });
            }
        }
    }

    private Set<FileChangeListener> matchedListeners(Path file) {
        return listeners.stream().filter(listener -> matchesPattern(file, listenerToFilePatternsMap.get(listener)))
                .collect(Collectors.toSet());
    }

    private boolean matchesPattern(Path input, Set<PathMatcher> patterns) {
        for (PathMatcher pattern : patterns) {
            if (pattern.matches(input)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void run() {
        LOGGER.debug("Start watching log directory");

        while (!cancelPending.get()) {
            LOGGER.debug("isCancelled " + cancelPending.get());

            WatchKey key;
            try {
                key = watchService.take();
            } catch (InterruptedException e) {
                LOGGER.error(DirectoryWatcher.class.getSimpleName() + " interrupted");
                break;
            }

            notifyListeners(key);

            // Reset key to allow further events for this key to be
            // processed.
            key.reset();
        }

        cancelPending.set(false);
        LOGGER.debug("Stop watching log directory");
    }
}
