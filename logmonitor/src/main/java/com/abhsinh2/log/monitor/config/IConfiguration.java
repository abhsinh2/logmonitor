package com.abhsinh2.log.monitor.config;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

import com.abhsinh2.log.monitor.parser.ILogMessageParser;

/**
 *
 * @author abhsinh2
 *
 */
public interface IConfiguration {
    public Executor getExecutor();

    public Properties getRemoteTailerProperties();

    public ILogMessageParser getFileParser(String fileSimpleName);

    public Map<String, ILogMessageParser> getFilePatternParserMap();
}
