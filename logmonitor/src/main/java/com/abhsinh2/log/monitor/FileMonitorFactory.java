package com.abhsinh2.log.monitor;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.abhsinh2.log.monitor.config.IConfiguration;
import com.abhsinh2.log.monitor.config.ParsingUtility;
import com.abhsinh2.log.monitor.parser.IMessageParser;
import com.abhsinh2.log.monitor.parser.file.FileMonitor;
import com.abhsinh2.log.monitor.tail.local.LocalFileTailer;
import com.abhsinh2.log.monitor.tail.remote.RemoteFileTailer;
import com.abhsinh2.log.monitor.watcher.DirectoryWatcherService;

/**
 *
 * @author abhsinh2
 *
 */
public class FileMonitorFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileMonitorFactory.class);

    private IConfiguration configuration;

    public FileMonitorFactory(IConfiguration configuration) {
        this.configuration = configuration;
    }

    public IFileMonitor parseLocalFile(URI uri, IMessageParser messageParser) {
        LOGGER.debug("uri: {}", uri);
        return new FileMonitor(uri, configuration.getExecutor(), messageParser);
    }

    public IFileMonitor parseLocalFile(URI uri) {
        LOGGER.debug("uri: {}", uri);
        IMessageParser messageParser = ParsingUtility.getParser(configuration, getFileName(uri));
        return new FileMonitor(uri, configuration.getExecutor(), messageParser);
    }

    public IFileMonitor watchDirectory(URI uri, String[] filenamePattern) throws IOException {
        return new DirectoryWatcherService(uri, configuration.getExecutor(), filenamePattern,
                configuration.getFilePatternParserMap());
    }

    public IFileMonitor tailLocalFile(URI uri, IMessageParser messageParser) {
        LOGGER.debug("uri: {}", uri);
        return new LocalFileTailer(uri, configuration.getExecutor(), messageParser);
    }

    public IFileMonitor tailLocalFile(URI uri) {
        LOGGER.debug("uri: {}", uri);
        IMessageParser messageParser = ParsingUtility.getParser(configuration, getFileName(uri));
        return new LocalFileTailer(uri, configuration.getExecutor(), messageParser);
    }

    public IFileMonitor tailRemoteFile(URI uri, String password, String passphrase, IMessageParser messageParser)
            throws IOException {
        LOGGER.debug("uri: {}", uri);

        Properties properties = new Properties(configuration.getRemoteTailerProperties());

        if (StringUtils.isNotEmpty(password)) {
            properties.put(RemoteFileTailer.LOG_TAILER_SSH_PASSWORD_KEY, password);
        }
        if (StringUtils.isNotEmpty(passphrase)) {
            properties.put(RemoteFileTailer.LOG_TAILER_SSH_PASSPHRASE_KEY, passphrase);
        }

        return new RemoteFileTailer(uri, configuration.getExecutor(), messageParser, properties);
    }

    public IFileMonitor tailRemoteFile(URI uri, String password, String passphrase) throws IOException {
        LOGGER.debug("uri: {}", uri);

        Properties properties = new Properties(configuration.getRemoteTailerProperties());

        if (StringUtils.isNotEmpty(password)) {
            properties.put(RemoteFileTailer.LOG_TAILER_SSH_PASSWORD_KEY, password);
        }
        if (StringUtils.isNotEmpty(passphrase)) {
            properties.put(RemoteFileTailer.LOG_TAILER_SSH_PASSPHRASE_KEY, passphrase);
        }
        IMessageParser messageParser = ParsingUtility.getParser(configuration, getFileName(uri));
        return new RemoteFileTailer(uri, configuration.getExecutor(), messageParser, properties);
    }

    private String getFileName(URI uri) {
        String path = uri.getPath();
        LOGGER.debug("path: {}", path);
        File file = new File(path);
        return file.getName();
    }
}
