package com.abhsinh2.log.monitor.tail.local;

import java.io.File;
import java.net.URI;
import java.util.concurrent.Executor;

import org.apache.commons.io.input.Tailer;
import org.apache.commons.io.input.TailerListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.abhsinh2.log.monitor.AbstractFileMonitor;
import com.abhsinh2.log.monitor.parser.IMessageParser;
import com.abhsinh2.log.monitor.parser.model.Record;

/**
 *
 * @author abhsinh2
 *
 */
public class LocalFileTailer extends AbstractFileMonitor {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocalFileTailer.class);
    private final LogTailer logTailer;

    public LocalFileTailer(URI uri, Executor executor, IMessageParser messageParser) {
        super(uri, executor, messageParser);

        String path = uri.getPath();
        LOGGER.debug("path: {}", path);
        File file = new File(path);
        LOGGER.debug("file: {}", file);
        logTailer = new LogTailer(file);
    }

    private class LogTailer extends TailerListenerAdapter implements Runnable {
        private final Tailer tailer;
        File file;

        LogTailer(File file) {
            LOGGER.debug("LogTailer: {}", file);
            this.file = file;
            tailer = new Tailer(file, this);
        }

        @Override
        public void run() {
            LOGGER.debug("LogTailer.run");
            tailer.run();
        }

        @Override
        public void handle(String line) {
            Record record = parse(line);
            notifyListener(this.file.toURI(), record);
        }

        @Override
        public void handle(Exception e) {
            // TODO
        }
    }

    @Override
    public void start() {
        executor.execute(logTailer);
    }

    @Override
    public void stop() {
        logTailer.tailer.stop();
        super.stop();
    }
}
