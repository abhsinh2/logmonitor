package com.abhsinh2.log.monitor.parser;

/**
 * Interface to define message parser.
 *
 * @author abhsinh2
 *
 */
public interface IMessageParser {
    /**
     * Parse the given message and returns tokens.
     *
     * @param message
     * @return
     */
    public String[] parse(String message);

    /**
     * Labels for each token, can be used in UI
     *
     * @return
     */
    public String[] labels();
}
