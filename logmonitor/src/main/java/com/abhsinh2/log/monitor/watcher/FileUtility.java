package com.abhsinh2.log.monitor.watcher;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.filefilter.RegexFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.slf4j.Logger;

import com.abhsinh2.log.monitor.config.logger.LogMonitorLogger;

/**
 *
 * @author abhsinh2
 *
 */
public class FileUtility {
    private static final Logger LOGGER = LogMonitorLogger.getLogger(FileUtility.class);

    public static List<File> getAllLogFiles(File rootDirectory) {
        List<File> result = new ArrayList<>();
        if (rootDirectory.isDirectory()) {
            for (File file : rootDirectory.listFiles()) {
                if (file.isFile()) {
                    result.add(file);
                }
            }
        }
        return result;
    }

    public static List<File> getAllLogFilesUsingRegex(File rootDirectory, String pattern) {
        List<File> result = new ArrayList<>();

        FileFilter filter = new RegexFileFilter(pattern);
        File[] files = rootDirectory.listFiles(filter);

        if (files != null) {
            for (File file : files) {
                if (file.isFile()) {
                    result.add(file);
                }
            }
        }
        return result;
    }

    public static List<File> getAllLogFilesUsingWildcard(File rootDirectory, String pattern) {
        List<File> result = new ArrayList<>();

        FileFilter filter = new WildcardFileFilter(pattern);
        File[] files = rootDirectory.listFiles(filter);

        if (files != null) {
            for (File file : files) {
                if (file.isFile()) {
                    result.add(file);
                }
            }
        }
        return result;
    }

    public static void getFiles(File rootDirectory, String pattern) throws IOException {
        try (Stream<Path> stream = Files.list(Paths.get(""))) {
            String joined = stream.map(String::valueOf).filter(path -> !path.startsWith(".")).sorted()
                    .collect(Collectors.joining("; "));
            LOGGER.debug("List {}", joined);
        }
    }

    public static void findFile(File rootDirectory) throws IOException {
        Path start = Paths.get(rootDirectory.getPath());
        int maxDepth = 5;
        try (Stream<Path> stream = Files.find(start, maxDepth, (path, attr) -> String.valueOf(path).endsWith(".js"))) {
            String joined = stream.sorted().map(String::valueOf).collect(Collectors.joining("; "));
            LOGGER.debug("Found {}", joined);
        }

        try (Stream<Path> stream = Files.walk(start, maxDepth)) {
            String joined = stream.map(String::valueOf).filter(path -> path.endsWith(".js")).sorted()
                    .collect(Collectors.joining("; "));
            LOGGER.debug("walk() {}", joined);
        }
    }

    public static void readWriteFiles(String filePath) throws IOException {
        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            stream.filter(line -> line.contains("print")).map(String::trim).forEach(LOGGER::debug);
        }

        Path path = Paths.get(filePath);
        try (BufferedReader reader = Files.newBufferedReader(path)) {
            LOGGER.debug(reader.readLine());
        }

        path = Paths.get(filePath);
        try (BufferedWriter writer = Files.newBufferedWriter(path)) {
            writer.write("print('Hello World');");
        }

        path = Paths.get(filePath);
        try (BufferedReader reader = Files.newBufferedReader(path)) {
            long countPrints = reader.lines().filter(line -> line.contains("print")).count();
            LOGGER.debug("" + countPrints);
        }
    }

}
