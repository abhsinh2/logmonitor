package com.abhsinh2.log.monitor;

import java.net.URI;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;

import com.abhsinh2.log.monitor.handler.Handler;
import com.abhsinh2.log.monitor.parser.IMessageParser;
import com.abhsinh2.log.monitor.parser.listener.ILogMessageListener;
import com.abhsinh2.log.monitor.parser.model.Label;
import com.abhsinh2.log.monitor.parser.model.Record;

/**
 *
 * @author abhsinh2
 *
 */
public abstract class AbstractFileMonitor implements IFileMonitor {

    private List<ILogMessageListener> logMessageListener = new CopyOnWriteArrayList<>();
    private List<Handler> handlers = new CopyOnWriteArrayList<>();

    protected final URI uri;
    protected final Executor executor;
    protected IMessageParser messageParser;

    public AbstractFileMonitor(URI uri, Executor executor) {
        this.uri = uri;
        this.executor = executor;
    }

    public AbstractFileMonitor(URI uri, Executor executor, IMessageParser messageParser) {
        this(uri, executor);
        this.messageParser = messageParser;
    }

    @Override
    public void addListener(ILogMessageListener listener) {
        if (listener != null) {
            logMessageListener.add(listener);
        }
    }

    @Override
    public void removeListener(ILogMessageListener listener) {
        if (listener != null) {
            logMessageListener.remove(listener);
        }
    }

    @Override
    public void addHandler(Handler handler) {
        if (handler != null) {
            handlers.add(handler);
        }
    }

    @Override
    public void removeHandler(Handler handler) {
        if (handler != null) {
            handlers.remove(handler);
        }
    }

    @Override
    public void stop() {
        resetHandler();
    }

    protected void notifyListener(URI uri, Record record) {
        for (ILogMessageListener logMessageListener : logMessageListener) {
            logMessageListener.listen(uri, record);
        }

        for (Handler handler : handlers) {
            handler.publish(record);
        }
    }

    protected void resetHandler() {
        for (Handler handler : handlers) {
            handler.close();
        }
    }

    protected Record parse(String message) {
        if (messageParser != null) {
            Label label = new Label(messageParser.labels());
            Record record = new Record(message, label, messageParser.parse(message));
            return record;
        }
        return null;
    }

}
