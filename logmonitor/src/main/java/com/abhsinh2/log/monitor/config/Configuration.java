package com.abhsinh2.log.monitor.config;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

import com.abhsinh2.log.monitor.executor.DefaultExecutor;
import com.abhsinh2.log.monitor.parser.ILogMessageParser;
import com.abhsinh2.log.monitor.tail.remote.RemoteFileTailer;

/**
 * Read configuration given in json file.
 *
 * @author abhsinh2
 *
 */
public class Configuration implements IConfiguration {
    private static ParsingConfiguration parsingConfiguration;
    private static ExecutorService executorService;

    public Configuration(File configFile) throws IOException {
        parsingConfiguration = ParsingConfiguration.getInstance();
        parsingConfiguration.readFile(configFile);

        Properties propertes = new Properties();
        propertes.put("executors", parsingConfiguration.getNumberOfExecutors());

        executorService = new DefaultExecutor(propertes);

        // ExecutorService executor = Executors.newFixedThreadPool(10, new
        // DaemonThreadFactory());
    }

    public Executor getExecutor() {
        return executorService;
    }

    public Properties getRemoteTailerProperties() {
        Properties properties = new Properties();
        properties.put(RemoteFileTailer.LOG_TAILER_SSH_PASSWORD_KEY, parsingConfiguration.getRemotePassword());
        properties.put(RemoteFileTailer.LOG_TAILER_SSH_PASSPHRASE_KEY, parsingConfiguration.getRemotePassphrase());
        properties.put(RemoteFileTailer.LOG_TAILER_SSH_TAIL_COMMAND_KEY, parsingConfiguration.getTailCommand());
        properties.put(RemoteFileTailer.LOG_TAILER_SSH_PRIVATE_KEY_FILE_PATH_KEY,
                parsingConfiguration.getPrivateKeyFilePath());
        return properties;
    }

    public ILogMessageParser getFileParser(String fileSimpleName) {
        // TODO: Need to know by WildFilter
        return parsingConfiguration.getFileParser(fileSimpleName);
    }

    public Map<String, ILogMessageParser> getFilePatternParserMap() {
        return parsingConfiguration.getFilePatternParserMap();
    }
}
