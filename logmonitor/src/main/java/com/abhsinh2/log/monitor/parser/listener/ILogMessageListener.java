package com.abhsinh2.log.monitor.parser.listener;

import java.net.URI;

import com.abhsinh2.log.monitor.parser.model.Record;

/**
 *
 * @author abhsinh2
 *
 */
public interface ILogMessageListener {
    public void listen(URI logFile, Record record);
}
