package com.abhsinh2.log.monitor.parser.listener;

/**
 * Listener to listen exception.
 *
 * @author abhsinh2
 *
 */
public interface IExceptionListener {
    public void handleException(Exception ex);
}
