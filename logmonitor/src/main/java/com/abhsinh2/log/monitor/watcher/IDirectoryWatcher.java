package com.abhsinh2.log.monitor.watcher;

import java.io.File;

/**
 *
 * @author abhsinh2
 *
 */
public interface IDirectoryWatcher {
    public void start();

    public void stop();

    public File getDirectory();

    void register(FileChangeListener listener, String... filePatterns);

    void register(FileChangeListener listener);

    interface FileChangeListener {
        default void onFileCreate(File file) {
        }

        default void onFileModify(File file) {
        }

        default void onFileDelete(File file) {
        }
    }
}
