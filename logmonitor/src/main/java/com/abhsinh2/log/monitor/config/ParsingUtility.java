package com.abhsinh2.log.monitor.config;

import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;

import com.abhsinh2.log.monitor.config.logger.LogMonitorLogger;
import com.abhsinh2.log.monitor.parser.ILogMessageParser;

/**
 *
 * @author abhsinh2
 *
 */
public class ParsingUtility {
    private static final Logger LOGGER = LogMonitorLogger.getLogger(ParsingUtility.class);

    public static ILogMessageParser getParser(Map<String, ILogMessageParser> fileParsers, String filename) {
        for (Entry<String, ILogMessageParser> fileParser : fileParsers.entrySet()) {
            String logFileExpression = fileParser.getKey();
            ILogMessageParser parser = fileParser.getValue();

            if (filename.matches(logFileExpression)) {
                LOGGER.debug("Returing Matching parser for {} : {}", filename, parser);
                return parser;
            }
        }

        LOGGER.error("No Matching parser for {}", filename);
        return null;
    }

    public static ILogMessageParser getParser(IConfiguration configuration, String filename) {
        return getParser(configuration.getFilePatternParserMap(), filename);
    }
}
