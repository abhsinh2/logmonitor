package com.abhsinh2.log.monitor.handler;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import com.abhsinh2.log.monitor.formatter.Formatter;
import com.abhsinh2.log.monitor.parser.model.Record;

/**
 *
 * @author abhsinh2
 *
 */
public class StreamHandler extends Handler {

    private OutputStream output;
    private boolean doneHeader;
    private volatile Writer writer;

    public StreamHandler() {

    }

    public StreamHandler(OutputStream out, Formatter formatter) {
        setFormatter(formatter);
        setOutputStream(out);
    }

    protected synchronized void setOutputStream(OutputStream out) {
        if (out == null) {
            throw new NullPointerException();
        }
        close();
        output = out;
        doneHeader = false;
        writer = new OutputStreamWriter(output);
    }

    @Override
    public synchronized void publish(Record record) {
        if (!isLoggable(record)) {
            return;
        }

        String msg;
        try {
            msg = getFormatter().format(record);
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }

        try {
            if (!doneHeader) {
                writer.write(getFormatter().getHead(record.getLabel()));
                doneHeader = true;
            }
            writer.write(msg);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public synchronized void flush() {
        if (writer != null) {
            try {
                writer.flush();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public synchronized void close() {
        if (writer != null) {
            try {
                writer.write(getFormatter().getTail());
                writer.flush();
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            writer = null;
            output = null;
        }
    }

    @Override
    public boolean isLoggable(Record record) {
        if (writer == null || record == null) {
            return false;
        }
        return super.isLoggable(record);
    }

}
