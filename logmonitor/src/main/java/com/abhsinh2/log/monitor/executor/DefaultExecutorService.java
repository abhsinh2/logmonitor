package com.abhsinh2.log.monitor.executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author abhsinh2
 *
 */
public class DefaultExecutorService {
    static final Logger LOGGER = LoggerFactory.getLogger(DefaultExecutorService.class);

    private ExecutorService executor;

    public void initialized(int executors) {
        LOGGER.debug("Initialized");
        LOGGER.debug("executors: {}", executors);

        ThreadFactory daemonFactory = new DaemonThreadFactory();

        if (executors <= 1) {
            executor = Executors.newSingleThreadExecutor(daemonFactory);
        } else {
            executor = Executors.newFixedThreadPool(executors, daemonFactory);
        }
    }

    public void destroyed() {
        LOGGER.debug("Destroyed");
        executor.shutdownNow();
    }
}
