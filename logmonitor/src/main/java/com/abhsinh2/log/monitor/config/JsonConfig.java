package com.abhsinh2.log.monitor.config;

import java.util.List;

/**
 *
 * @author abhsinh2
 *
 */
public class JsonConfig {
    private LocalDirectoryWatcher localDirectoryWatcher;
    private TailRemoteFile tailRemoteFile;
    private String tailCommand;
    private List<Parser> parsers;
    private List<FilenamePattern> filenamePatterns;
    private int executors;

    public LocalDirectoryWatcher getLocalDirectoryWatcher() {
        return localDirectoryWatcher;
    }

    public void setLocalDirectoryWatcher(LocalDirectoryWatcher localDirectoryWatcher) {
        this.localDirectoryWatcher = localDirectoryWatcher;
    }

    public TailRemoteFile getTailRemoteFile() {
        return tailRemoteFile;
    }

    public void setTailRemoteFile(TailRemoteFile tailRemoteFile) {
        this.tailRemoteFile = tailRemoteFile;
    }

    public String getTailCommand() {
        return tailCommand;
    }

    public void setTailCommand(String tailCommand) {
        this.tailCommand = tailCommand;
    }

    public List<Parser> getParsers() {
        return parsers;
    }

    public void setParsers(List<Parser> parsers) {
        this.parsers = parsers;
    }

    public List<FilenamePattern> getFilenamePatterns() {
        return filenamePatterns;
    }

    public void setFilenamePatterns(List<FilenamePattern> filenamePatterns) {
        this.filenamePatterns = filenamePatterns;
    }

    public int getExecutors() {
        return executors;
    }

    public void setExecutors(int executors) {
        this.executors = executors;
    }

    static class LocalDirectoryWatcher {
        private String directory;
        private List<String> logFilesFilter;

        public String getDirectory() {
            return directory;
        }

        public void setDirectory(String directory) {
            this.directory = directory;
        }

        public List<String> getLogFilesFilter() {
            return logFilesFilter;
        }

        public void setLogFilesFilter(List<String> logFilesFilter) {
            this.logFilesFilter = logFilesFilter;
        }
    }

    static class TailRemoteFile {
        private String password;
        private String passphrase;
        private String privateKeyFilePath;

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getPassphrase() {
            return passphrase;
        }

        public void setPassphrase(String passphrase) {
            this.passphrase = passphrase;
        }

        public String getPrivateKeyFilePath() {
            return privateKeyFilePath;
        }

        public void setPrivateKeyFilePath(String privateKeyFilePath) {
            this.privateKeyFilePath = privateKeyFilePath;
        }
    }

    static class Parser {
        private String name;
        private String pattern;
        private String dateFormat;
        private int dateIndex;
        private List<String> labels;
        private String log4jPattern;
        private String className;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPattern() {
            return pattern;
        }

        public void setPattern(String pattern) {
            this.pattern = pattern;
        }

        public String getDateFormat() {
            return dateFormat;
        }

        public void setDateFormat(String dateFormat) {
            this.dateFormat = dateFormat;
        }

        public int getDateIndex() {
            return dateIndex;
        }

        public void setDateIndex(int dateIndex) {
            this.dateIndex = dateIndex;
        }

        public List<String> getLabels() {
            return labels;
        }

        public void setLabels(List<String> labels) {
            this.labels = labels;
        }

        public String getLog4jPattern() {
            return log4jPattern;
        }

        public void setLog4jPattern(String log4jPattern) {
            this.log4jPattern = log4jPattern;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }
    }

    static class FilenamePattern {
        private String name;
        private String parser;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getParser() {
            return parser;
        }

        public void setParser(String parser) {
            this.parser = parser;
        }
    }

}
