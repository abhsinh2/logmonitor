package com.abhsinh2.log.monitor.tail.remote;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.abhsinh2.log.monitor.AbstractFileMonitor;
import com.abhsinh2.log.monitor.parser.IMessageParser;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

/**
 *
 * @author abhsinh2
 *
 */
public class RemoteFileTailer extends AbstractFileMonitor {
    public static final String LOG_TAILER_SSH_PRIVATE_KEY_FILE_PATH_KEY = "privateKeyFilePath";
    public static final String LOG_TAILER_SSH_PASSWORD_KEY = "password";
    public static final String LOG_TAILER_SSH_PASSPHRASE_KEY = "passphrase";
    public static final String LOG_TAILER_SSH_TAIL_COMMAND_KEY = "tailCommand";

    private static final Logger LOGGER = LoggerFactory.getLogger(RemoteFileTailer.class);

    private TailerSsh tailer;

    public RemoteFileTailer(URI uri, Executor executor, IMessageParser messageParser, Properties properties,
            TailerSsh tailer) throws IOException {
        super(uri, executor, messageParser);
        this.tailer = tailer;

        initTailer(properties);
    }

    public RemoteFileTailer(URI uri, Executor executor, IMessageParser messageParser, Properties properties)
            throws IOException {
        super(uri, executor, messageParser);

        JSch jsch = new JSch();
        tailer = new TailerSsh(jsch, uri, executor);

        initTailer(properties);
    }

    private void initTailer(Properties properties) throws IOException {
        String privateKeyFilePath = properties.getProperty(LOG_TAILER_SSH_PRIVATE_KEY_FILE_PATH_KEY);
        LOGGER.debug("privateKeyFilePath: {}", privateKeyFilePath);

        if (privateKeyFilePath != null) {
            String passphrase = properties.getProperty(LOG_TAILER_SSH_PASSPHRASE_KEY);
            LOGGER.debug("passphrase found: {}", passphrase != null);
            try {
                if (passphrase != null) {
                    tailer.getJsch().addIdentity(privateKeyFilePath, passphrase);
                } else {
                    tailer.getJsch().addIdentity(privateKeyFilePath);
                }
            } catch (JSchException e) {
                throw new IOException(e);
            }
        }

        String password = properties.getProperty(LOG_TAILER_SSH_PASSWORD_KEY);
        LOGGER.debug("password found: {}", password != null);
        if (password != null) {
            tailer.getSession().setPassword(password);
        }

        tailer.getSession().setConfig("StrictHostKeyChecking", "no");

        String tailCommand = properties.getProperty(LOG_TAILER_SSH_TAIL_COMMAND_KEY);
        if (tailCommand != null) {
            LOGGER.debug("tailCommand: {}", tailCommand);
            tailer.setTailCommand(tailCommand);
        }

        LOGGER.debug("a");
    }

    @Override
    public void start() {
        LOGGER.debug("Start");
        tailer.start();
    }

    @Override
    public void stop() {
        tailer.stop();
        super.stop();
    }

    public TailerSsh getTailer() {
        return tailer;
    }

    class TailerSsh {
        private static final String DEFAULT_TAIL_COMMAND = "tail -F";

        private final Executor executor;
        private final JSch jsch;
        private final CountDownLatch completed;

        private String tailCommand;
        private Session session;
        private String filePath;
        private volatile boolean stopping;
        private volatile Thread inputThread;
        private volatile Thread errorThread;

        TailerSsh(JSch jsch, URI uri, Executor executor) throws IOException {
            this.jsch = jsch;
            this.executor = executor;
            completed = new CountDownLatch(1);

            tailCommand = DEFAULT_TAIL_COMMAND;
            stopping = false;

            LOGGER.debug("uri: {}", uri);
            String user = uri.getUserInfo();
            LOGGER.debug("user: {}", user);
            String host = uri.getHost();
            LOGGER.debug("host: {}", host);
            String uriPath = uri.getPath();
            LOGGER.debug("uriPath: {}", uriPath);
            filePath = new File(uri.getPath()).getCanonicalPath();
            LOGGER.debug("filePath: {}", filePath);

            try {
                session = jsch.getSession(user, host);
            } catch (JSchException e) {
                throw new IOException(e);
            }
        }

        public JSch getJsch() {
            return jsch;
        }

        public Session getSession() {
            return session;
        }

        public void setTailCommand(String tailCommand) {
            this.tailCommand = tailCommand;
        }

        public void start() {
            LOGGER.debug("start");
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    inputThread = Thread.currentThread();
                    remoteTailWrapped();
                }
            };
            executor.execute(runnable);
        }

        public void stop() {
            LOGGER.debug("stop");
            stopping = true;
            if (errorThread != null) {
                errorThread.interrupt();
            }
            if (inputThread != null) {
                inputThread.interrupt();
            }
        }

        void await() throws InterruptedException {
            completed.await();
        }

        private void remoteTailWrapped() {
            LOGGER.debug("start remoteTailWrapped");
            try {
                remoteTail();
            } catch (Exception e) {
                LOGGER.debug("exception", e);
                if (!stopping) {
                    // TODO: tailerListener.handleException(e);
                }
            } finally {
                completed.countDown();
            }
            LOGGER.debug("end remoteTailWrapped");
        }

        private void remoteTail() throws Exception {
            session.connect();

            ChannelExec channel = (ChannelExec) session.openChannel("exec");

            String command = String.format("%s %s", tailCommand, filePath);
            LOGGER.debug("command: {}", command);
            channel.setCommand(command);

            channel.setInputStream(null);

            InputStream errStream = channel.getErrStream();
            InputStream inputStream = channel.getInputStream();

            channel.connect();
            LOGGER.debug("channel connected");

            drainErrorStream(errStream);
            drainInputStream(inputStream);

            channel.disconnect();
            session.disconnect();
        }

        private void drainInputStream(final InputStream inputStream) throws IOException {
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(inputStream));
            while (!stopping) {
                String line = inputReader.readLine();
                if (line == null) {
                    break;
                }
                // TODO: tailerListener.handleLine(line);
            }
            LOGGER.debug("input stream drained");
        }

        private void drainErrorStream(final InputStream errStream) {
            Runnable drainer = new Runnable() {
                @Override
                public void run() {
                    errorThread = Thread.currentThread();
                    BufferedReader errReader = new BufferedReader(new InputStreamReader(errStream));

                    while (true) {
                        String line;
                        try {
                            line = errReader.readLine();
                        } catch (IOException e) {
                            LOGGER.debug("exception", e);
                            return;
                        }
                        if (line == null) {
                            break;
                        }
                        LOGGER.error("remote process error: {}", line);
                        // TODO: tailerListener.handleLine(String.format("[%s]",
                        // line));
                    }
                    LOGGER.debug("error stream drained");
                }
            };
            executor.execute(drainer);
        }
    }

}
