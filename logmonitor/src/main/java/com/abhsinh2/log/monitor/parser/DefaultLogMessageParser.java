package com.abhsinh2.log.monitor.parser;

/**
 * Default log message parser.
 *
 * @author abhsinh2
 *
 */
public class DefaultLogMessageParser extends AbstractLogMessageParser {

    private static final String[] DEFAULT_LABELS = new String[] { "Time", "ThreadName", "Severity", "Message" };
    private static final String DEFAULT_LOG_PATTERN = "\\[\\s*(.+)\\s*\\]\\s+\\[\\s*(.+)\\s*\\]\\s+\\[\\s*(.+)\\s*\\]\\s+\\[\\s*(.+)\\s*\\]\\s+-\\s+(.+)";
    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss,SSS";
    private static final int DEFAULT_DATE_INDEX = 0;

    /**
     * default labels, log message regex and date format will be used.
     *
     */
    public DefaultLogMessageParser() {
        this(DEFAULT_LOG_PATTERN, DEFAULT_LABELS, DEFAULT_DATE_FORMAT, DEFAULT_DATE_INDEX);
    }

    public DefaultLogMessageParser(String logPattern, String[] labels, String dateFormat, int dateIndex) {
        super(logPattern, labels, dateFormat, dateIndex);

        if (dateFormat == null) {
            this.dateFormat = DEFAULT_DATE_FORMAT;
        } else {
            this.dateFormat = dateFormat;
        }
    }

    @Override
    public int getDateIndex() {
        return dateIndex;
    }
}
