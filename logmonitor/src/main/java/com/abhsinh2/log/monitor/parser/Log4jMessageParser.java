package com.abhsinh2.log.monitor.parser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.helpers.OptionConverter;
import org.apache.log4j.pattern.ClassNamePatternConverter;
import org.apache.log4j.pattern.DatePatternConverter;
import org.apache.log4j.pattern.FileLocationPatternConverter;
import org.apache.log4j.pattern.FullLocationPatternConverter;
import org.apache.log4j.pattern.LevelPatternConverter;
import org.apache.log4j.pattern.LineLocationPatternConverter;
import org.apache.log4j.pattern.LineSeparatorPatternConverter;
import org.apache.log4j.pattern.LiteralPatternConverter;
import org.apache.log4j.pattern.LoggerPatternConverter;
import org.apache.log4j.pattern.LoggingEventPatternConverter;
import org.apache.log4j.pattern.MessagePatternConverter;
import org.apache.log4j.pattern.MethodLocationPatternConverter;
import org.apache.log4j.pattern.NDCPatternConverter;
import org.apache.log4j.pattern.PatternParser;
import org.apache.log4j.pattern.PropertiesPatternConverter;
import org.apache.log4j.pattern.RelativeTimePatternConverter;
import org.apache.log4j.pattern.SequenceNumberPatternConverter;
import org.apache.log4j.pattern.ThreadPatternConverter;

/**
 * Log4j message parser.
 *
 * @author abhsinh2
 *
 */
public class Log4jMessageParser extends DefaultLogMessageParser {

    private static final String DEFAULT_LOG4J_PATTERN = "[%d] [%t] [%c{1}] [%-5p] - %m%n";
    // private static final String DEFAULT_LOG4J_PATTERN = "[%d{yyyy-MM-dd}]
    // [%t] [%c{1}] [%-5p] - %m%n";

    private String log4jPattern;
    private ArrayList<String> calculatedLabels = new ArrayList<>();
    private int dateIndex = 0;

    public Log4jMessageParser() {
        this(DEFAULT_LOG4J_PATTERN, null);
    }

    public Log4jMessageParser(String log4jPattern, String[] labels) {
        if (log4jPattern != null) {
            this.log4jPattern = log4jPattern;
        } else {
            this.log4jPattern = DEFAULT_LOG4J_PATTERN;
        }

        this.labels = labels;

        this.logPattern = convertToJavaRegex(this.log4jPattern);
        if (this.labels == null) {
            this.labels = calculatedLabels.toArray(new String[calculatedLabels.size()]);
        }

        this.dateFormat = convertDateFormatString();
    }

    protected String convertToJavaRegex(String log4jRegex) {
        String input = OptionConverter.convertSpecialChars(log4jRegex);
        List<?> converters = new ArrayList<>();
        List<?> fields = new ArrayList<>();
        Map<?, ?> converterRegistry = null;

        PatternParser.parse(input, converters, fields, converterRegistry, PatternParser.getPatternLayoutRules());
        return getFormat(converters);
    }

    @SuppressWarnings("unused")
    private String getFormat1(List<?> converters) {
        StringBuffer sb = new StringBuffer();
        int count = 0;

        for (Iterator<?> iter = converters.iterator(); iter.hasNext();) {
            LoggingEventPatternConverter converter = (LoggingEventPatternConverter) iter.next();
            if (converter instanceof DatePatternConverter) {
                this.dateIndex = count;
                calculatedLabels.add("Date");
                sb.append("(.+)");
            } else if (converter instanceof MessagePatternConverter) {
                calculatedLabels.add("Message");
                sb.append("(.+)");
            } else if (converter instanceof LoggerPatternConverter) {
                calculatedLabels.add("Logger");
                sb.append("(.+)");
            } else if (converter instanceof ClassNamePatternConverter) {
                calculatedLabels.add("Class");
                sb.append("(.+)");
            } else if (converter instanceof RelativeTimePatternConverter) {
                calculatedLabels.add("RelativeTime");
                sb.append("(.+)");
            } else if (converter instanceof ThreadPatternConverter) {
                calculatedLabels.add("Thread");
                sb.append("(.+)");
            } else if (converter instanceof NDCPatternConverter) {
                calculatedLabels.add("NDC");
                sb.append("(.+)");
            } else if (converter instanceof LiteralPatternConverter) {
                LiteralPatternConverter literal = (LiteralPatternConverter) converter;
                literal.format(null, sb);
            } else if (converter instanceof SequenceNumberPatternConverter) {
                calculatedLabels.add("Log4jId");
                sb.append("(.+)");
            } else if (converter instanceof LevelPatternConverter) {
                calculatedLabels.add("Level");
                sb.append("(.+)");
            } else if (converter instanceof MethodLocationPatternConverter) {
                calculatedLabels.add("Method");
                sb.append("(.+)");
            } else if (converter instanceof FullLocationPatternConverter) {
                calculatedLabels.add("Location");
                sb.append("(.+)");
            } else if (converter instanceof LineLocationPatternConverter) {
                calculatedLabels.add("LineLocation");
                sb.append("(.+)");
            } else if (converter instanceof FileLocationPatternConverter) {
                calculatedLabels.add("File");
                sb.append("(.+)");
            } else if (converter instanceof PropertiesPatternConverter) {
                calculatedLabels.add("Properties");
                sb.append("(.+)");
            } else if (converter instanceof LineSeparatorPatternConverter) {

            } else {

            }

            count++;
        }

        String regex = sb.toString().trim();
        regex = regex.replaceAll("\\[", "\\\\[");
        regex = regex.replaceAll("\\]", "\\\\]");
        regex = regex.replaceAll(" ", "\\\\s");
        return regex;
    }

    private String getFormat(List<?> converters) {
        StringBuffer sb = new StringBuffer();
        int count = 0;

        for (Iterator<?> iter = converters.iterator(); iter.hasNext();) {
            LoggingEventPatternConverter converter = (LoggingEventPatternConverter) iter.next();

            if (converter instanceof DatePatternConverter || converter instanceof MessagePatternConverter
                    || converter instanceof LoggerPatternConverter || converter instanceof ClassNamePatternConverter
                    || converter instanceof RelativeTimePatternConverter || converter instanceof ThreadPatternConverter
                    || converter instanceof NDCPatternConverter || converter instanceof SequenceNumberPatternConverter
                    || converter instanceof LevelPatternConverter || converter instanceof MethodLocationPatternConverter
                    || converter instanceof FullLocationPatternConverter
                    || converter instanceof LineLocationPatternConverter
                    || converter instanceof FileLocationPatternConverter
                    || converter instanceof PropertiesPatternConverter) {
                sb.append("(.+)");
                calculatedLabels.add(converter.getName());

                if (converter instanceof DatePatternConverter) {
                    this.dateIndex = count;
                }

                count++;
            } else if (converter instanceof LiteralPatternConverter) {
                LiteralPatternConverter literal = (LiteralPatternConverter) converter;
                literal.format(null, sb);
            }
        }

        String regex = sb.toString().trim();
        regex = regex.replaceAll("\\[", "\\\\[");
        regex = regex.replaceAll("\\]", "\\\\]");
        regex = regex.replaceAll(" ", "\\\\s");
        return regex;
    }

    @Override
    public int getDateIndex() {
        return dateIndex;
    }

    @Override
    public SimpleDateFormat getDateFormat() {
        return new SimpleDateFormat(this.dateFormat);
    }

    private String convertDateFormatString() {
        Matcher dateFormatMatcher = Pattern.compile("%-?\\d*(?:\\.\\d+)?d(?:\\{([^}]+)\\})?").matcher(this.log4jPattern);
        String dateFormat = "yyyy-MM-dd HH:mm:ss,SSS"; // ISO8601
        if (dateFormatMatcher.find() && dateFormatMatcher.groupCount() >= 1) {
            if (dateFormatMatcher.group(1) != null) {
                if (dateFormatMatcher.group(1).equals("ABSOLUTE")) {
                    dateFormat = "HH:mm:ss,SSS";
                } else {
                    dateFormat = dateFormatMatcher.group(1);
                }
            }
        }
        return dateFormat;
    }
}
