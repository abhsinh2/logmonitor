package com.abhsinh2.log.monitor.watcher;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;

import org.slf4j.Logger;

import com.abhsinh2.log.monitor.AbstractFileMonitor;
import com.abhsinh2.log.monitor.config.ParsingUtility;
import com.abhsinh2.log.monitor.config.logger.LogMonitorLogger;
import com.abhsinh2.log.monitor.parser.ILogMessageParser;
import com.abhsinh2.log.monitor.parser.file.DefaultFileParser;
import com.abhsinh2.log.monitor.parser.file.IFileParser;
import com.abhsinh2.log.monitor.parser.listener.ILogMessageListener;
import com.abhsinh2.log.monitor.parser.model.Record;

/**
 *
 * @author abhsinh2
 *
 */
public class DirectoryWatcherService extends AbstractFileMonitor {
    private static final Logger LOGGER = LogMonitorLogger.getLogger(DirectoryWatcherService.class);

    private IDirectoryWatcher watchService;
    private Set<File> logsToParse = new HashSet<>();
    private Date startDate;
    private Date endDate;
    private Map<String, ILogMessageParser> fileParsers;

    public DirectoryWatcherService(URI uri, Executor executor, String[] filenamePattern,
            Map<String, ILogMessageParser> fileParsers) throws IOException {
        super(uri, executor);
        this.fileParsers = fileParsers;

        String path = uri.getPath();
        LOGGER.debug("path: {}", path);
        File directory = new File(path);
        LOGGER.debug("directory: {}", directory);

        if (!directory.isDirectory()) {
            throw new IllegalArgumentException(directory + " is not a directory.");
        }

        watchService = new DirectoryWatcher(directory, executor);
        watchService.register(new DirectoryWatcher.FileChangeListener() {
            @Override
            public void onFileCreate(File file) {
                LOGGER.debug("File created ", file);
                logsToParse.add(file);
            }

            @Override
            public void onFileModify(File file) {
                LOGGER.debug("File modified {}", file);
                logsToParse.add(file);
            }

            @Override
            public void onFileDelete(File file) {
                LOGGER.debug("File deleted {}", file);
                logsToParse.add(file);
            }
        }, filenamePattern);
    }

    @Override
    public void start() {
        this.startDate = Calendar.getInstance().getTime();
        LOGGER.info("Start Monitoring at {}", this.startDate);

        watchService.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            stop();
            LOGGER.error("Start thread interrupted.");
        }
    }

    @Override
    public void stop() {
        this.endDate = Calendar.getInstance().getTime();
        LOGGER.debug("Stop Monitoring at {}", this.endDate);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            LOGGER.error("Stop thread interrupted.");
        }

        // Stop DirectoryWatcher
        watchService.stop();
        startOfflineParsing();
        resetHandler();
    }

    private void startOfflineParsing() {
        LOGGER.debug("Start parsing logs");
        LOGGER.debug("Files to parse {}", logsToParse);

        try {
            retrieveLogs(startDate, endDate, logsToParse, new ILogMessageListener() {
                @Override
                public void listen(URI uri, Record record) {
                    notifyListener(uri, record);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void retrieveLogs(Date startDate, Date endDate, Collection<File> logFiles,
            ILogMessageListener logMessageListener) throws Exception {

        for (File logFile : logFiles) {
            ILogMessageParser parser = ParsingUtility.getParser(fileParsers, logFile.getName());

            if (parser != null) {
                IFileParser fileParser = new DefaultFileParser();
                fileParser.listen(logFile, parser, startDate, endDate, logMessageListener);
            }
        }
    }

}
