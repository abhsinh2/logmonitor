package com.abhsinh2.log.monitor.action;

/**
 *
 * @author abhsinh2
 *
 */
public class CommandMessage {
    public enum Command {
        TAIL_LOCAL_FILE, TAIL_REMOTE_FILE, WATCH_LOCAL_DIRECTORY, PARSE_LOCAL_FILE, STOP, RESPONSE_STARTED, RESPONSE_STOPPED, RESPONSE_EXCEPTION
    }

    public Command action;
    public String localFile;
    public String remoteFile;
    public String password;
    public String passphrase;
    public String directory;
    public String[] filePattern;
}
