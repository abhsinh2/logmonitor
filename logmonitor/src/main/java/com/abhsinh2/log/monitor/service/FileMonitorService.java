package com.abhsinh2.log.monitor.service;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.abhsinh2.log.monitor.FileMonitorFactory;
import com.abhsinh2.log.monitor.IFileMonitor;
import com.abhsinh2.log.monitor.action.CommandMessage;
import com.abhsinh2.log.monitor.action.CommandMessage.Command;
import com.abhsinh2.log.monitor.action.MessageResponder;
import com.abhsinh2.log.monitor.config.IConfiguration;
import com.abhsinh2.log.monitor.handler.Handler;
import com.abhsinh2.log.monitor.parser.listener.ILogMessageListener;

/**
 *
 * @author abhsinh2
 *
 */
public class FileMonitorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileMonitorService.class);

    private MessageResponder messageSender;
    private FileMonitorFactory fileMonitorFactory;

    private final ObjectMapper mapper = new ObjectMapper();

    private IFileMonitor fileMonitor;
    private List<Handler> handlers;
    private List<ILogMessageListener> listeners;

    public FileMonitorService(FileMonitorFactory fileMonitorFactory, MessageResponder messageSender) {
        this.messageSender = messageSender;
        this.fileMonitorFactory = fileMonitorFactory;
    }

    public FileMonitorService(IConfiguration configuration, MessageResponder messageSender) {
        this(new FileMonitorFactory(configuration), messageSender);
    }

    public void setHandlers(List<Handler> handlers) {
        this.handlers = handlers;
    }

    public void setListeners(List<ILogMessageListener> listeners) {
        this.listeners = listeners;
    }

    public void handleMessage(String messageString) {
        LOGGER.debug("messageString: " + messageString);
        CommandMessage message;
        try {
            message = mapper.readValue(messageString, CommandMessage.class);
        } catch (IOException e) {
            LOGGER.error("message format error", e);
            onException(e);
            return;
        }

        LOGGER.debug("message.action: " + message.action);

        try {
            switch (message.action) {
            case TAIL_LOCAL_FILE:
                tailLocalFile(message);
                onMessage(Command.RESPONSE_STARTED, "");
                break;
            case TAIL_REMOTE_FILE:
                tailRemoteFile(message);
                onMessage(Command.RESPONSE_STARTED, "");
                break;
            case WATCH_LOCAL_DIRECTORY:
                watchLocalDirectory(message);
                onMessage(Command.RESPONSE_STARTED, "");
                break;
            case PARSE_LOCAL_FILE:
                parseLocalFile(message);
                onMessage(Command.RESPONSE_STARTED, "");
                break;
            case STOP:
                close();
                break;
            default:
                LOGGER.error("unexpected message action");
            }
        } catch (Exception e) {
            onException(e);
        }
    }

    private void parseLocalFile(CommandMessage message) throws Exception {
        String filename = message.localFile;
        LOGGER.debug("filename: {}", filename);

        File file = new File(filename);
        URI uriLocal;
        try {
            uriLocal = new URI("file", "///" + file.getAbsolutePath(), null);
        } catch (URISyntaxException e) {
            throw new Exception(e);
        }
        fileMonitor = fileMonitorFactory.parseLocalFile(uriLocal);
        addHandlersAndListeners();
        fileMonitor.start();
    }

    private void tailLocalFile(CommandMessage message) throws Exception {
        String filename = message.localFile;
        LOGGER.debug("filename: {}", filename);

        File file = new File(filename);
        URI uriLocal;
        try {
            uriLocal = new URI("file", "///" + file.getAbsolutePath(), null);
        } catch (URISyntaxException e) {
            throw new Exception(e);
        }
        fileMonitor = fileMonitorFactory.tailLocalFile(uriLocal);
        addHandlersAndListeners();
        fileMonitor.start();
    }

    private void tailRemoteFile(CommandMessage message) throws Exception {
        String uriString = message.remoteFile;
        LOGGER.debug("uriString: {}", uriString);

        URI uriRemote;
        try {
            uriRemote = new URI(uriString);
        } catch (URISyntaxException e) {
            throw new Exception(e);
        }
        fileMonitor = fileMonitorFactory.tailRemoteFile(uriRemote, message.password, message.passphrase);
        addHandlersAndListeners();
        fileMonitor.start();
    }

    private void watchLocalDirectory(CommandMessage message) throws Exception {
        String uriString = message.directory;
        LOGGER.debug("uriString: {}", uriString);

        URI uriRemote;
        try {
            uriRemote = new URI(uriString);
        } catch (URISyntaxException e) {
            throw new Exception(e);
        }
        fileMonitor = fileMonitorFactory.watchDirectory(uriRemote, message.filePattern);
        addHandlersAndListeners();
        fileMonitor.start();
    }

    private void close() {
        LOGGER.debug("close");
        if (fileMonitor != null) {
            fileMonitor.stop();
            onMessage(Command.RESPONSE_STOPPED, "");
        }
        fileMonitor = null;
    }

    private void onMessage(Command action, String message) {
        sendResponse(action, message);
    }

    private void onException(Exception e) {
        sendResponse(Command.RESPONSE_EXCEPTION, String.format("[%s]", e.getMessage()));
        // fileMonitor.stop();
    }

    private void sendResponse(Command action, String message) {
        CommandMessage response = new CommandMessage();
        response.action = action;

        try {
            messageSender.receive(mapper.writeValueAsString(response));
        } catch (IOException e) {
            LOGGER.debug("exception", e);
            throw new RuntimeException(e);
        }
    }

    private void addHandlersAndListeners() {
        if (handlers != null) {
            for (Handler handler : handlers) {
                this.fileMonitor.addHandler(handler);
            }
        }

        if (listeners != null) {
            for (ILogMessageListener listener : listeners) {
                this.fileMonitor.addListener(listener);
            }
        }
    }

}
