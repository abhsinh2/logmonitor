package com.abhsinh2.log.monitor.parser.model;

/**
 *
 * @author abhsinh2
 *
 */
public final class Record {
    private String log;
    private Label label;
    private String[] tokens;

    public Record(String log, Label label, String[] tokens) {
        this.log = log;
        this.label = label;
        this.tokens = tokens;
    }

    public String[] getTokens() {
        return tokens;
    }

    public Label getLabel() {
        return label;
    }

    public String getLog() {
        return log;
    }
}
