package com.abhsinh2.log.monitor.parser.file;

import java.io.File;
import java.util.Date;

import com.abhsinh2.log.monitor.parser.ILogMessageParser;
import com.abhsinh2.log.monitor.parser.IMessageParser;
import com.abhsinh2.log.monitor.parser.listener.ILogMessageListener;

/**
 * Interface to parse file.
 *
 * @author abhsinh2
 *
 */
public interface IFileParser {
    public void listen(File fileTobeParsed, IMessageParser messageParser, ILogMessageListener messageListener)
            throws FileParserException;

    public void listen(File fileTobeParsed, ILogMessageParser messageParser, Date startDate, Date endDate,
            ILogMessageListener messageListener) throws FileParserException;
}
