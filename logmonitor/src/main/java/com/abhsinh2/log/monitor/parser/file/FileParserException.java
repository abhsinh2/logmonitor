package com.abhsinh2.log.monitor.parser.file;

/**
 *
 * @author abhsinh2
 *
 */
public class FileParserException extends Exception {
    private static final long serialVersionUID = 1L;

    public FileParserException(String message) {
        super(message);
    }
}
