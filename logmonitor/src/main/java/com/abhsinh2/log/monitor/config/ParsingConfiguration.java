package com.abhsinh2.log.monitor.config;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import com.abhsinh2.log.monitor.parser.DefaultLogMessageParser;
import com.abhsinh2.log.monitor.parser.ILogMessageParser;
import com.abhsinh2.log.monitor.parser.Log4jMessageParser;
import com.google.gson.Gson;

/**
 * Read configuration given in json file.
 *
 * @author abhsinh2
 *
 */
public class ParsingConfiguration {
    private static ParsingConfiguration instance;

    private File localWatchDirectory;
    private String[] logFilesFilter;

    private String remotePassword;
    private String remotePassphrase;
    private String privateKeyFilePath;

    private String tailCommand;

    private Map<String, ILogMessageParser> filePatternParserMap = new HashMap<>();

    private static AtomicBoolean isRunning = new AtomicBoolean(false);
    private static boolean alreayRan = false;

    private static JsonConfig config;

    private ParsingConfiguration() {

    }

    public static ParsingConfiguration getInstance() {
        if (instance == null) {
            instance = new ParsingConfiguration();
        }
        return instance;
    }

    public void readFile(File configFile) throws IOException {
        if (isRunning.compareAndSet(false, true) && !alreayRan) {
            Gson gson = new Gson();
            config = gson.fromJson(new FileReader(configFile), JsonConfig.class);

            populateDirectoryWatcher();
            populateRemoteFile();
            populateTailCommand();
            populateFileParsers();

            isRunning.set(false);
            alreayRan = true;
        }
    }

    private void populateDirectoryWatcher() {
        JsonConfig.LocalDirectoryWatcher watcher = config.getLocalDirectoryWatcher();
        localWatchDirectory = new File(watcher.getDirectory());
        logFilesFilter = watcher.getLogFilesFilter().toArray(new String[watcher.getLogFilesFilter().size()]);
    }

    private void populateRemoteFile() {
        JsonConfig.TailRemoteFile remoteFile = config.getTailRemoteFile();
        remotePassword = remoteFile.getPassword();
        remotePassphrase = remoteFile.getPassphrase();
    }

    private void populateTailCommand() {
        tailCommand = config.getTailCommand();
    }

    private void populateFileParsers() {
        List<JsonConfig.FilenamePattern> filenamePatterns = config.getFilenamePatterns();
        if (filenamePatterns != null) {
            for (JsonConfig.FilenamePattern filePattern : filenamePatterns) {
                JsonConfig.Parser parser = getParserByName(filePattern.getParser());
                if (parser == null) {
                    filePatternParserMap.put(filePattern.getName(), new DefaultLogMessageParser());
                } else {
                    filePatternParserMap.put(filePattern.getName(), getLogMessageParser(parser));
                }
            }
        }
    }

    private ILogMessageParser getLogMessageParser(JsonConfig.Parser parser) {
        if (parser.getClassName() != null) {
            try {
                return (ILogMessageParser) Class.forName(parser.getClassName()).newInstance();
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        if (parser.getLog4jPattern() != null) {
            return new Log4jMessageParser(parser.getLog4jPattern(),
                    parser.getLabels().toArray(new String[parser.getLabels().size()]));
        }

        if (parser.getPattern() != null) {
            new DefaultLogMessageParser(parser.getPattern(),
                    parser.getLabels().toArray(new String[parser.getLabels().size()]), parser.getDateFormat(),
                    parser.getDateIndex());
        }

        return null;
    }

    private JsonConfig.Parser getParserByName(String name) {
        List<JsonConfig.Parser> parsers = config.getParsers();
        if (parsers != null) {
            for (JsonConfig.Parser parser : parsers) {
                if (parser.getName().equals(name)) {
                    return parser;
                }
            }
        }
        return null;
    }

    public Map<String, ILogMessageParser> getFilePatternParserMap() {
        return filePatternParserMap;
    }

    public File getWatcherDirectory() {
        return localWatchDirectory;
    }

    public String[] getLogFilesFilter() {
        return logFilesFilter;
    }

    public String getRemotePassword() {
        return remotePassword;
    }

    public String getRemotePassphrase() {
        return remotePassphrase;
    }

    public String getTailCommand() {
        return tailCommand;
    }

    public int getNumberOfExecutors() {
        return config.getExecutors();
    }

    public ILogMessageParser getFileParser(String fileSimpleName) {
        return filePatternParserMap.get(fileSimpleName);
    }

    public String getPrivateKeyFilePath() {
        return config.getTailRemoteFile().getPrivateKeyFilePath();
    }
}
