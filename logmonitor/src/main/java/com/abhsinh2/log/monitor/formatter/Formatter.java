package com.abhsinh2.log.monitor.formatter;

import com.abhsinh2.log.monitor.parser.model.Label;
import com.abhsinh2.log.monitor.parser.model.Record;

/**
 *
 * @author abhsinh2
 *
 */
public abstract class Formatter {

    protected Formatter() {
    }

    public abstract String format(Record record);

    public String getHead(Label label) {
        return "";
    }

    public String getTail() {
        return "";
    }
}
