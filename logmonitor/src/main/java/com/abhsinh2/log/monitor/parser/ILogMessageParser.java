package com.abhsinh2.log.monitor.parser;

import java.text.SimpleDateFormat;

/**
 * Interface to define log4j message parser.
 *
 * @author abhsinh2
 *
 */
public interface ILogMessageParser extends IMessageParser {
    public enum LogType {
        INFO("INFO"), DEBUG("DEBUG"), ERROR("ERROR"), WARN("WARN");

        private String severity;

        LogType(String severity) {
            this.severity = severity;
        }

        public String getSeverity() {
            return this.severity;
        }
    }

    /**
     * Return date format used in message to be parsed.
     *
     * @return
     */
    public SimpleDateFormat getDateFormat();

    /**
     * Return index for date in log message.
     *
     * @return
     */
    public int getDateIndex();
}
