package com.abhsinh2.log.monitor.tail.local;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.io.PrintStream;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.abhsinh2.log.monitor.AbstractTestCase;
import com.abhsinh2.log.monitor.executor.DaemonThreadFactory;
import com.abhsinh2.log.monitor.parser.DefaultLogMessageParser;
import com.abhsinh2.log.monitor.parser.listener.ILogMessageListener;
import com.abhsinh2.log.monitor.parser.model.Record;

@RunWith(MockitoJUnitRunner.class)
public class LocalFileTailerTest extends AbstractTestCase {

    private DaemonThreadFactory daemonThreadFactory = new DaemonThreadFactory();

    public LocalFileTailerTest() {
        super(LocalFileTailerTest.class);
    }

    @Test
    public void testTailingLocalFile() throws Exception {

        final List<String> lines = new CopyOnWriteArrayList<String>();
        ILogMessageListener logMessageListener = new ILogMessageListener() {
            @Override
            public void listen(URI logFile, Record record) {
                lines.add(record.getLog());
            }
        };

        File tempFile = File.createTempFile("abc", "log");
        tempFile.deleteOnExit();
        URI uri = tempFile.toURI();
        Executor executor = Executors.newFixedThreadPool(10, daemonThreadFactory);

        LocalFileTailer localFileTailer = new LocalFileTailer(uri, executor, new DefaultLogMessageParser());
        localFileTailer.addListener(logMessageListener);

        localFileTailer.start();

        PrintStream printStream = new PrintStream(tempFile);

        printStream.println("[2016-08-25 00:22:31,249] [pool-1-thread-3] [inventory] [INFO] - Some Info 1");
        printStream.flush();
        Thread.sleep(2000);
        assertThat(lines,
                is(Arrays.asList("[2016-08-25 00:22:31,249] [pool-1-thread-3] [inventory] [INFO] - Some Info 1")));

        printStream.println("[2016-08-25 00:22:31,260] [pool-1-thread-3] [inventory] [INFO] - Some Info 2");
        printStream.flush();
        Thread.sleep(2000);
        assertThat(lines,
                is(Arrays.asList("[2016-08-25 00:22:31,249] [pool-1-thread-3] [inventory] [INFO] - Some Info 1",
                        "[2016-08-25 00:22:31,260] [pool-1-thread-3] [inventory] [INFO] - Some Info 2")));

        printStream.close();

        localFileTailer.stop();
    }

}
