package com.abhsinh2.log.monitor.misc;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Test {

    static long count = 1;

    public static void main(String[] args) throws Exception {
        List<String> result = new ArrayList<>();

        final Pattern p = Pattern.compile(
                "\\[\\s*(.+)\\s*\\]\\s+\\[\\s*(.+)\\s*\\]\\s+\\[\\s*(.+)\\s*\\]\\s+\\[\\s*(.+)\\s*\\]\\s+-\\s+(.+)");

        String ss = "This is line ";

        File root = new File(System.getProperty("user.dir"));
        File logFile = new File(root, "src/test/resources/logs/sample.log");

        try (Stream<String> stream = Files.lines(Paths.get(logFile.getPath()))) {
            stream.forEach(str -> {
                Matcher m = p.matcher(str);
                m.matches();

                StringBuffer sb = new StringBuffer();
                sb.append("[");
                sb.append(m.group(1));
                sb.append("] ");
                sb.append("[");
                sb.append(m.group(2));
                sb.append("] ");
                sb.append("[");
                sb.append(m.group(3));
                sb.append("] ");
                sb.append("[");
                sb.append(m.group(4));
                sb.append("] ");
                sb.append("- ");
                sb.append(ss + count);

                result.add(sb.toString());
                count++;
            });
        }

        File outputFile = new File(root, "src/test/resources/logs/output.log");

        FileWriter writer = new FileWriter(outputFile);
        for (String str : result) {
            writer.write(str);
            writer.write("\n");
        }
        writer.close();
    }

}
