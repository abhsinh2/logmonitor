package com.abhsinh2.log.monitor.tail.remote;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

import java.io.File;
import java.net.URI;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.abhsinh2.log.monitor.AbstractTestCase;
import com.abhsinh2.log.monitor.parser.DefaultLogMessageParser;
import com.abhsinh2.log.monitor.parser.listener.ILogMessageListener;
import com.abhsinh2.log.monitor.parser.model.Record;
import com.abhsinh2.log.monitor.tail.remote.RemoteFileTailer.TailerSsh;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

@RunWith(MockitoJUnitRunner.class)
public class RemoteFileTailerTest extends AbstractTestCase {

    private Properties properties = mock(Properties.class);

    public RemoteFileTailerTest() {
        super(RemoteFileTailerTest.class);
    }

    @Test
    public void testTailingRemoteFile() throws Exception {
        File pkFile = new File(RemoteFileTailerTest.class.getClassLoader().getResource("test_private_key").toURI());
        String pkFilePath = pkFile.getCanonicalPath();

        given(properties.getProperty(RemoteFileTailer.LOG_TAILER_SSH_PRIVATE_KEY_FILE_PATH_KEY)).willReturn(pkFilePath);

        // String passphrase = "ok!";
        // given(properties.getProperty(RemoteFileTailer.LOG_TAILER_SSH_PASSPHRASE_KEY)).willReturn(passphrase);

        String password = "1strong2";
        given(properties.getProperty(RemoteFileTailer.LOG_TAILER_SSH_PASSWORD_KEY)).willReturn(password);

        String tailCommand = "tail -F";
        given(properties.getProperty(RemoteFileTailer.LOG_TAILER_SSH_TAIL_COMMAND_KEY)).willReturn(tailCommand);

        final List<String> lines = new CopyOnWriteArrayList<String>();
        ILogMessageListener logMessageListener = new ILogMessageListener() {
            @Override
            public void listen(URI logFile, Record record) {
                lines.add(record.getLog());
            }
        };

        URI uri = new URI("ssh://root@10.104.242.70/root/test.log");
        Executor executor = mock(Executor.class);

        TailerSsh tailer = mock(TailerSsh.class);
        JSch jsch = mock(JSch.class);
        Session session = mock(Session.class);

        given(tailer.getJsch()).willReturn(jsch);
        given(tailer.getSession()).willReturn(session);

        RemoteFileTailer remoteFileTailer = new RemoteFileTailer(uri, executor, new DefaultLogMessageParser(),
                properties, tailer);
        // given(remoteFileTailer.getTailer()).willReturn(tailer);

        remoteFileTailer.addListener(logMessageListener);
        remoteFileTailer.start();

        // verify(jsch).addIdentity(pkFilePath, passphrase);
        doNothing().when(executor).execute(any(Runnable.class));
    }
}
