package com.abhsinh2.log.monitor.watcher;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintStream;
import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.abhsinh2.log.monitor.AbstractTestCase;
import com.abhsinh2.log.monitor.config.IConfiguration;
import com.abhsinh2.log.monitor.executor.DaemonThreadFactory;
import com.abhsinh2.log.monitor.formatter.HtmlFormatter;
import com.abhsinh2.log.monitor.handler.FileHandler;
import com.abhsinh2.log.monitor.handler.Handler;
import com.abhsinh2.log.monitor.parser.ILogMessageParser;
import com.abhsinh2.log.monitor.parser.Log4jMessageParser;
import com.abhsinh2.log.monitor.parser.listener.ILogMessageListener;
import com.abhsinh2.log.monitor.parser.model.Record;

@RunWith(MockitoJUnitRunner.class)
public class DirectoryWatcherServiceTest extends AbstractTestCase {
    private DaemonThreadFactory daemonThreadFactory = new DaemonThreadFactory();

    private IConfiguration configuration = mock(IConfiguration.class);

    public DirectoryWatcherServiceTest() {
        super(DirectoryWatcherServiceTest.class);
    }

    @Test
    public void test() {
        final List<String> lines = new CopyOnWriteArrayList<String>();
        ILogMessageListener logMessageListener = new ILogMessageListener() {
            @Override
            public void listen(URI logFile, Record record) {
                lines.add(record.getLog());
            }
        };

        File rootDirectory = new File(System.getProperty("user.dir") + "/src/test/resources/logs");
        File file = new File(System.getProperty("user.dir") + "/src/test/resources/logs/test.log");

        Executor executor = Executors.newFixedThreadPool(10, daemonThreadFactory);

        Map<String, ILogMessageParser> fileParsers = new HashMap<>();
        fileParsers.put("test.log", new Log4jMessageParser());

        try {
            Handler htmlFileHandler = new FileHandler("abc.html");
            htmlFileHandler.setFormatter(new HtmlFormatter("Test"));

            DirectoryWatcherService watcher = new DirectoryWatcherService(rootDirectory.toURI(), executor,
                    new String[] { "*.log" }, fileParsers);
            watcher.addHandler(htmlFileHandler);
            watcher.addListener(logMessageListener);
            watcher.start();

            LOGGER.debug("Writing to {}", file.getPath());

            writeToFile(file, 10);
            // writeToFile1(file);

            // closing watcher
            watcher.stop();

            assertTrue(lines.size() == 10);
        } catch (Exception ex) {

        } finally {
            LOGGER.debug("Deleting file");
            file.delete();
        }
    }

    private void writeToFile(File file, int count) throws Exception {
        try {
            LOGGER.info("Writing to file {}", file.getPath());

            // if file doesn't exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS");
            for (int i = 0; i < count; i++) {
                bw.write("[" + dateFormat.format(new Date()) + "]"
                        + " [JobsExecutor-14] [jobmanager] [DEBUG] - This is line " + i);
                bw.write("\n");
                Thread.sleep(1000);
            }
            fw.flush();
            bw.close();
            fw.close();
        } catch (Exception e) {
            LOGGER.error("", e);
        }
    }

    private void writeToFile1(File file, int count) throws Exception {
        // Writing to temp file
        PrintStream printStream = new PrintStream(file);

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS");
        String str = null;

        for (int i = 0; i < count; i++) {
            str = "[" + dateFormat.format(new Date()) + "]" + " [JobsExecutor-14] [jobmanager] [DEBUG] - This is line "
                    + i;
            printStream.println(str);
            Thread.sleep(1000);
        }

        printStream.flush();
        printStream.close();
    }

}
