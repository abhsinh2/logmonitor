package com.abhsinh2.log.monitor.parser;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.abhsinh2.log.monitor.AbstractTestCase;

public class Log4jMessageParserTest extends AbstractTestCase {

    public Log4jMessageParserTest() {
        super(Log4jMessageParserTest.class);
    }

    @Test
    public void test() {
        String log = "[2016-08-25 00:22:31,249] [pool-1-thread-3] [inventory] [INFO] - Populating the collection hooks";
        String logPattern = "[%d] [%t] [%c{1}] [%-5p] - %m%n";
        Log4jMessageParser parser = new Log4jMessageParser(logPattern, null);

        Assert.assertEquals(0, parser.getDateIndex());

        String[] messages = parser.parse(log);

        Assert.assertEquals(5, messages.length);
        Assert.assertEquals("2016-08-25 00:22:31,249", messages[0]);
        Assert.assertEquals("pool-1-thread-3", messages[1]);
        Assert.assertEquals("inventory", messages[2]);
        Assert.assertEquals("INFO", messages[3]);
        Assert.assertEquals("Populating the collection hooks", messages[4]);
    }
}
