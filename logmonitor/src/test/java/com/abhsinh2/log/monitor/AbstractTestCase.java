package com.abhsinh2.log.monitor;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;

import org.slf4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import com.abhsinh2.log.monitor.config.logger.LogMonitorLogger;

public abstract class AbstractTestCase {
    protected static String configFilePath;
    protected boolean initializeApp = true;

    protected static Logger LOGGER;

    public AbstractTestCase(Class<? extends AbstractTestCase> clazz) {
        File rootFile = new File(System.getProperty("user.dir"));
        File logDirectory = new File(rootFile, "logs");

        System.setProperty("log.monitor.logging.dir", rootFile.getPath());
        LOGGER = LogMonitorLogger.getLogger(clazz);
    }

    @BeforeTest
    public void beforeTest() {
        File rootFile = new File(System.getProperty("user.dir"));
        File configFile = new File(rootFile, "src/test/resources/conf/log_regex_config.json");
        configFilePath = configFile.getPath();
    }

    @BeforeClass
    public void beforeClass() {
    }

    @BeforeMethod
    public void beforeMethod() {
    }

    @AfterMethod
    public void afterMethod() {
    }

    @AfterClass
    public void afterClass() {
    }

    @AfterTest
    public void afterTest() {
    }

    protected void createDummyFile(String filename) throws IOException {
        File root = new File(System.getProperty("user.dir"));
        File dummyZipFile = new File(root, filename);
        if (!dummyZipFile.exists()) {
            dummyZipFile.createNewFile();
        }
    }

    protected void deleteDummypFile(String filename) {
        File root = new File(System.getProperty("user.dir"));
        File dummyZipFile = new File(root, filename);
        if (dummyZipFile.exists()) {
            dummyZipFile.delete();
        }
    }

    protected void createDummyFolder(String filename) throws IOException {
        File root = new File(System.getProperty("user.dir"));
        File dummyZipFolder = new File(root, filename);
        if (!dummyZipFolder.exists()) {
            dummyZipFolder.createNewFile();
        }
    }

    protected void deleteDummyFolder(String filename) throws IOException {
        File root = new File(System.getProperty("user.dir"));
        File dummyZipFolder = new File(root, filename);
        if (dummyZipFolder.exists()) {
            Path rootPath = Paths.get(dummyZipFolder.getPath());
            Files.walk(rootPath, FileVisitOption.FOLLOW_LINKS).sorted(Comparator.reverseOrder()).map(Path::toFile)
                    .peek(System.out::println).forEach(File::delete);
        }
    }
}
