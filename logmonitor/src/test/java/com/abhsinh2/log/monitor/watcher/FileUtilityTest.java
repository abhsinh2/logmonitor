package com.abhsinh2.log.monitor.watcher;

import java.io.File;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.abhsinh2.log.monitor.AbstractTestCase;

public class FileUtilityTest extends AbstractTestCase {
    public FileUtilityTest() {
        super(FileUtilityTest.class);
    }

    @Test
    public void getAllLogFiles() {
        File dir = new File("src/test/resources/logs");
        List<File> file = FileUtility.getAllLogFiles(dir);
        Assert.assertEquals(file.size(), 3);
    }

    @Test
    public void getAllLogFilesUsingRegex() {
        File dir = new File("src/test/resources/logs");
        List<File> file = FileUtility.getAllLogFilesUsingRegex(dir, ".*[.]log");
        Assert.assertEquals(3, file.size());
        file = FileUtility.getAllLogFilesUsingRegex(dir, "sample.*[.]log");
        Assert.assertEquals(2, file.size());
        file = FileUtility.getAllLogFilesUsingRegex(dir, "sample[.]log");
        Assert.assertEquals(1, file.size());
    }

    @Test
    public void getAllLogFilesUsingWildcard() {
        File dir = new File("src/test/resources/logs");
        List<File> file = FileUtility.getAllLogFilesUsingWildcard(dir, "*.log");
        Assert.assertEquals(3, file.size());
        file = FileUtility.getAllLogFilesUsingWildcard(dir, "sample*.log");
        Assert.assertEquals(2, file.size());
    }
}
