package com.abhsinh2.log.monitor;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.concurrent.Executor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.abhsinh2.log.monitor.config.IConfiguration;
import com.abhsinh2.log.monitor.parser.file.FileMonitor;
import com.abhsinh2.log.monitor.tail.local.LocalFileTailer;
import com.abhsinh2.log.monitor.tail.remote.RemoteFileTailer;
import com.abhsinh2.log.monitor.watcher.DirectoryWatcherService;

@RunWith(MockitoJUnitRunner.class)
public class FileMonitorFactoryTest {
    @Mock
    private Executor executor;

    @Mock
    private IConfiguration configuration;

    @InjectMocks
    private FileMonitorFactory fileMonitorFactory;

    @Test
    public void parseLocalFile() throws IOException {
        when(configuration.getExecutor()).thenReturn(executor);
        URI uri = new File("src/test/resources/logs/test.log").toURI();
        IFileMonitor fileMonitor = fileMonitorFactory.parseLocalFile(uri, null);
        assertThat(fileMonitor, instanceOf(FileMonitor.class));
    }

    @Test
    public void watchDirectory() throws IOException {
        when(configuration.getExecutor()).thenReturn(executor);
        URI uri = new File("src/test/resources/logs").toURI();
        IFileMonitor fileMonitor = fileMonitorFactory.watchDirectory(uri, new String[] { "*.log" });
        assertThat(fileMonitor, instanceOf(DirectoryWatcherService.class));
    }

    @Test
    public void tailLocalFile() throws IOException {
        when(configuration.getExecutor()).thenReturn(executor);
        URI uri = new File("src/test/resources/logs/test.log").toURI();
        IFileMonitor fileMonitor = fileMonitorFactory.tailLocalFile(uri, null);
        assertThat(fileMonitor, instanceOf(LocalFileTailer.class));
    }

    @Test
    public void tailRemoteFile() throws Exception {
        when(configuration.getExecutor()).thenReturn(executor);
        URI uri = new URI("ssh://root@1.1.1.1/root/file.log");
        String password = null;
        String passphrase = null;
        IFileMonitor fileMonitor = fileMonitorFactory.tailRemoteFile(uri, password, passphrase, null);
        assertThat(fileMonitor, instanceOf(RemoteFileTailer.class));
    }
}
