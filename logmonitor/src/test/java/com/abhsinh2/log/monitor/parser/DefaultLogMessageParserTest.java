package com.abhsinh2.log.monitor.parser;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.abhsinh2.log.monitor.AbstractTestCase;

/**
 *
 * @author abhsinh2
 *
 */
public class DefaultLogMessageParserTest extends AbstractTestCase {

    public DefaultLogMessageParserTest() {
        super(DefaultLogMessageParserTest.class);
    }

    private DefaultLogMessageParser parser = new DefaultLogMessageParser();

    @Test
    public void test() {
        String log = "[2016-08-25 00:22:31,249] [pool-1-thread-3] [inventory] [INFO] - Populating the collection hooks";

        String[] messages = parser.parse(log);
        Assert.assertEquals(5, messages.length);
        Assert.assertEquals("2016-08-25 00:22:31,249", messages[0]);
        Assert.assertEquals("pool-1-thread-3", messages[1]);
        Assert.assertEquals("inventory", messages[2]);
        Assert.assertEquals("INFO", messages[3]);
        Assert.assertEquals("Populating the collection hooks", messages[4]);
    }

    @Test
    public void testError() {
        String log = "[2016-08-25 00:22:38,360] [pool-1-thread-5] [inventory] [ERROR] - Exception Stack: com.cisco.xmp.jobmanager.logging.JobManagerXMPCheckedException: The Task Element Specification is invalid Trigger cannot be null.-JOBM_002"
                + "at com.cisco.xmp.jobmanager.logging.Logging.getXMPCheckedException(Logging.java:138)"
                + "at com.cisco.xmp.jobapprover.advice.JobApproverAdvice.handleAddJob(JobApproverAdvice.java:158)"
                + "at com.cisco.xmp.jobmanager.jobimpl.JobManagerServiceImpl.addJob(JobManagerServiceImpl.java:636)"
                + "at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)"
                + "at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)"
                + "at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)"
                + "at java.lang.reflect.Method.invoke(Method.java:497)"
                + "at org.springframework.aop.support.AopUtils.invokeJoinpointUsingReflection(AopUtils.java:302)"
                + "at org.springframework.aop.framework.JdkDynamicAopProxy.invoke(JdkDynamicAopProxy.java:202)"
                + "at com.sun.proxy.$Proxy87.addJob(Unknown Source)"
                + "at com.cisco.xmp.inventory.adaptors.InventoryCollectorJobServiceImpl.createInventoryCollectionJobWithTrigger(InventoryCollectorJobServiceImpl.java:216)"
                + "at com.cisco.xmp.inventory.postStart.IcePostInitHookImpl.postInitialize(IcePostInitHookImpl.java:71)"
                + "at com.cisco.xmp.startup.XMPMain$InitInitializers.run(XMPMain.java:454)"
                + "at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)"
                + "at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)"
                + "at java.lang.Thread.run(Thread.java:745)";

        String[] messages = parser.parse(log);
        Assert.assertEquals(5, messages.length);
        Assert.assertEquals("2016-08-25 00:22:38,360", messages[0]);
        Assert.assertEquals("pool-1-thread-5", messages[1]);
        Assert.assertEquals("inventory", messages[2]);
        Assert.assertEquals("ERROR", messages[3]);
        Assert.assertTrue(messages[4].contains("getXMPCheckedException"));
    }
}
