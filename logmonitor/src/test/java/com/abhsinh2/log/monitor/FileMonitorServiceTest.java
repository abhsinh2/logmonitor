package com.abhsinh2.log.monitor;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.net.URI;
import java.util.Properties;
import java.util.concurrent.Executor;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.runners.MockitoJUnitRunner;

import com.abhsinh2.log.monitor.action.CommandMessage;
import com.abhsinh2.log.monitor.action.CommandMessage.Command;
import com.abhsinh2.log.monitor.action.MessageResponder;
import com.abhsinh2.log.monitor.config.IConfiguration;
import com.abhsinh2.log.monitor.service.FileMonitorService;

@RunWith(MockitoJUnitRunner.class)
public class FileMonitorServiceTest {
    Properties properties = mock(Properties.class);
    IConfiguration configuration = mock(IConfiguration.class);
    MessageResponder messageSender = mock(MessageResponder.class);
    FileMonitorFactory fileMonitorFactory = mock(FileMonitorFactory.class);

    FileMonitorService fileMonitorManager = new FileMonitorService(fileMonitorFactory, messageSender);

    final ObjectMapper mapper = new ObjectMapper();

    @Test
    public void tailLocalFile() throws IOException {
        // given
        String filename = "abc.log";
        CommandMessage message = new CommandMessage();
        message.action = Command.TAIL_LOCAL_FILE;
        message.localFile = filename;
        String tailLocalFileMessage = mapper.writeValueAsString(message);

        Executor executor = mock(Executor.class);
        given(configuration.getExecutor()).willReturn(executor);

        IFileMonitor fileMonitor = mock(IFileMonitor.class);
        given(fileMonitorFactory.tailLocalFile(any(URI.class))).willReturn(fileMonitor);

        // when
        fileMonitorManager.handleMessage(tailLocalFileMessage);

        // then
        verify(fileMonitor).start();

        // then
        ArgumentCaptor<String> responseString = ArgumentCaptor.forClass(String.class);
        verify(messageSender).receive(responseString.capture());
        CommandMessage sentMessage = mapper.readValue(responseString.getValue(), CommandMessage.class);
        assertThat(sentMessage.action, is(Command.RESPONSE_STARTED));

        // and given
        String closeLogMessage = createCloseMessage();
        fileMonitorManager.handleMessage(closeLogMessage);
        verify(fileMonitor).stop();
    }

    @Test
    public void tailRemoteFile() throws IOException {
        // given
        String uriString = "ssh://hostname.com/path/filename.log";
        String password = null;
        String passphrase = "let me in";

        CommandMessage message = new CommandMessage();
        message.action = Command.TAIL_REMOTE_FILE;
        message.remoteFile = uriString;
        message.password = password;
        message.passphrase = passphrase;
        String tailRemoteFileMessage = mapper.writeValueAsString(message);

        IFileMonitor fileMonitor = mock(IFileMonitor.class);
        given(fileMonitorFactory.tailRemoteFile(any(URI.class), eq(password), eq(passphrase))).willReturn(fileMonitor);

        // when
        fileMonitorManager.handleMessage(tailRemoteFileMessage);

        // then
        verify(fileMonitor).start();

        // then
        ArgumentCaptor<String> responseString = ArgumentCaptor.forClass(String.class);
        verify(messageSender).receive(responseString.capture());
        CommandMessage sentMessage = mapper.readValue(responseString.getValue(), CommandMessage.class);
        assertThat(sentMessage.action, is(Command.RESPONSE_STARTED));

        // and given
        String closeLogMessage = createCloseMessage();
        fileMonitorManager.handleMessage(closeLogMessage);
        verify(fileMonitor).stop();
    }

    private String createCloseMessage() throws IOException {
        CommandMessage message = new CommandMessage();
        message.action = Command.STOP;
        return mapper.writeValueAsString(message);
    }
}
